/// <reference path="../libs/jquery/jquery.d.ts" />
/// <reference path="../libs/timeline/beeTimeline.ts" />
var Demo = (function () {
    function Demo(change_src_callback) {
        this.left_bicom = $('#left_time');
        this.right_bicom = $('#right_time');
        this.change_src_callback = change_src_callback;
        var el = document.getElementById('demo_timeline');
        this.timeline = new BTL(el, new TimeSpan(SpanType.MINUTE, 10));
        this.setBiconsTime();
        this.checkTimeBiconTextPosition();
        this.checkTimeBiconTextPosition();
        this.registerEvents();
    }
    Demo.prototype.addMotionEvents = function (events) {
        this.timeline.AddItems(events);
    };

    Demo.prototype.removeMotionEvents = function () {
        // TODO: IMPLEMENT THIS
		this.timeline.RemoveItems();
    };

    Demo.prototype.moveCursor = function (video_time) {
        if (isNaN(video_time)) {
            return;
        }
        this.timeline.SetCursorTime(new Date(video_time));
    };

    Demo.prototype.setBiconsTime = function () {
        var time = this.timeline.GetTimeBorders();
        var left = moment(time[0]).format("MMM D");
        var right = moment(time[1]).format("MMM D");

        if (this.last_left_time !== left) {
            this.last_left_time = left;
            this.left_bicom.text(left);
        }
        if (this.last_right_time !== right) {
            this.last_right_time = right;
            this.right_bicom.text(right);
        }
    };

    Demo.prototype.checkTimeBiconTextPosition = function () {
        var w = $(window).width();
        var margin_val = 12;
        if (w < 664) {
            margin_val = 2;
        }
        $('.time_bicon_p').css('margin-top', margin_val + 'px');
    };

    Demo.prototype.registerEvents = function () {
        var _this = this;
        $(document).on("cursorChange", function (e) {
            var data = e["time"];
            console.log("cursorChange", e, data);
            _this.change_src_callback(data);
        });

        $('.stage_change').on("click", function (e) {
            var val = e.target.innerHTML;
            if (val === "10min") {
                _this.timeline.SetTimeSpan(SpanType.MINUTE, 10);
            } else if (val === "1h") {
                _this.timeline.SetTimeSpan(SpanType.HOUR, 1);
            } else if (val === "24h") {
                _this.timeline.SetTimeSpan(SpanType.HOUR, 24);
            } else if (val === "7d") {
                _this.timeline.SetTimeSpan(SpanType.DAY, 7);
            } else if (val === "1mon") {
                _this.timeline.SetTimeSpan(SpanType.MONTH, 1);
            }
            $(".stage_change").removeClass("selected");
            $(e.target).addClass("selected");
            _this.setBiconsTime();
        });
        jQuery(document).on('timelineTimeChange', function (e) {
            _this.setBiconsTime();
        });

        $(window).on('resize', function (e) {
            _this.checkTimeBiconTextPosition();
        });
    };

    Demo.prototype.MoveWindowToCursor = function (time) {
        this.timeline.MoveWindowToCursor(time);
    };
    return Demo;
})();
//# sourceMappingURL=demo.js.map
