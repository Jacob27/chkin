/**
 * Created by neeqola on 6.7.14..
 */


function getVLC(id) {
    return document.getElementById(id);
}

function registerVLCEvent(vlc, event, handler) {
    // var vlc = getVLC("vlc");
    if (vlc) {
        if (vlc.attachEvent) {
            // Microsoft
            vlc.attachEvent (event, handler);
        } else if (vlc.addEventListener) {
            // Mozilla: DOM level 2
            vlc.addEventListener (event, handler, false);
        } else {
            // DOM level 0
            vlc["on" + event] = handler;
        }
    }
}

// stop listening to event
function unregisterVLCEvent(event, handler) {
    var vlc = getVLC("vlc");
    if (vlc) {
        if (vlc.detachEvent) {
            // Microsoft
            vlc.detachEvent (event, handler);
        } else if (vlc.removeEventListener) {
            // Mozilla: DOM level 2
            vlc.removeEventListener (event, handler, false);
        } else {
            // DOM level 0
            vlc["on" + event] = null;
        }
    }
}

// event callback function for testing
function handleEvents(event) {
    if (!event) event = window.event; // IE
    if (!event) {
        console.log("handleEvents(null)")
        return
    }
    if (event.target) {
        // Netscape based browser
        targ = event.target;
    } else if (event.srcElement) {
        // ActiveX
        targ = event.srcElement;
    } else {
        // No event object, just the value
        console.log("Event value" + event );
        return;
    }
    if (targ.nodeType == 3) // defeat Safari bug
        targ = targ.parentNode;
    console.log("Event " + event.type + " has fired from " + targ );
}
// handle mouse grab event from video filter
function handleMouseGrab(event,X,Y) {
    if (!event) event = window.event; // IE
    console.log("new position (" + X + "," + Y + ")");
}

function start_test_later_if_plugin_is_not_allowed() {
    var vlc = getVLC("vlc");
    if(vlc == undefined || vlc.playlist == undefined) {
        console.log("delay ...");
        window.setTimeout(start_test_later_if_plugin_is_not_allowed, 1000);
        return;
    }
    // Register a bunch of callbacks.

    registerVLCEvent('MediaPlayerEndReached', function() {console.log("MediaPlayerEndReached")} );
    registerVLCEvent('MediaPlayerPositionChanged', function() {console.log("MediaPlayerPositionChanged", getVLC("vlc").input.position * 100.0)} );

    // registerVLCEvent('MediaPlayerNothingSpecial', handleEvents);
    // registerVLCEvent('MediaPlayerOpening', handleEvents);
    // registerVLCEvent('MediaPlayerBuffering', handleEvents);
    // registerVLCEvent('MediaPlayerPlaying', handleEvents);
    // registerVLCEvent('MediaPlayerPaused', handleEvents);
    // registerVLCEvent('MediaPlayerForward', handleEvents);
    // registerVLCEvent('MediaPlayerBackward', handleEvents);
    // registerVLCEvent('MediaPlayerEncounteredError', handleEvents);
    // registerVLCEvent('MediaPlayerEndReached', handleEvents);
    // registerVLCEvent('MediaPlayerTimeChanged', handleEvents);
    // registerVLCEvent('MediaPlayerPositionChanged', handleEvents);
    // registerVLCEvent('MediaPlayerSeekableChanged', handleEvents);
    // registerVLCEvent('MediaPlayerPausableChanged', handleEvents);

    console.log("vlc", vlc);
    for(i=0; i<video_urls.length; i++) {
        var url = video_urls[i];
        vlc.playlist.add(url);
    }
    vlc.playlist.play();
}

function play(url, current_playitem_start, current_playitem_duration, is_live_stream) {
    // flash player logic:
    console.log("FLASH play", url)
    document.getElementById("IrisPlayer").play(url)

    // when current_playitem_start == null, current_playitem_duration == null this is live stream
    // var vlc_container = document.getElementById("_player_video")
    // vlc_container.innerHTML = '<embed id="vlc" windowless="true" type="application/x-vlc-plugin" pluginspage="http://www.videolan.org" width="100%" height="100%"></embed>';
    // window.setTimeout(function () {
    //     var vlc = getVLC("vlc");
    //     if(vlc == undefined || vlc.playlist == undefined) {
    //         console.log("VLC plugin unauthorized");
    //         return;
    //     }
    //     registerVLCEvent(vlc, 'MediaPlayerEndReached', function() {console.log("MediaPlayerEndReached")} );
    //     registerVLCEvent(vlc, 'MediaPlayerPositionChanged', function() {
    //         if(is_live_stream == true) {
    //             main.demo.moveCursor(new Date().getTime());
    //             return ;
    //         }
    //         console.log("MediaPlayerPositionChanged", vlc.input.position * 100.0 , " vlc reported length in ms=", vlc.input.length);
    //         if(current_playitem_start != undefined) {
    //             // var video_time = new Date(current_playitem_start.getTime() + (vlc.input.position * current_playitem_duration));
    //             var video_time = current_playitem_start.getTime() + (vlc.input.position * current_playitem_duration);
    //             console.log("position changed", video_time, "started:", current_playitem_start, "elapsed:", vlc.input.time);
    //             main.demo.moveCursor(video_time);
    //         } else {
    //             console.log("current_playitem_start == null")
    //         }
    //     } );
    //     var key = CookieUtils.readCookie("key");
    //     if (key == undefined || key == "undefined") {
    //         console.log("KEY IS UNDEFINED !!!");
    //     }
    //     var video_url = url
    //     if (is_live_stream != true) {
    //         video_url += "&key=" + key;
    //     }
    //     console.log("play_url:", video_url)
    //     vlc.playlist.add(video_url);
    //     vlc.playlist.play();
    // }, 0);
}