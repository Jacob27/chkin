import pytz
import urllib
import urllib2
import time

import webapp2
import main
from json import dumps, loads
from datetime import datetime
from logging import debug as d, info as i, warning as w, error as e, critical as c

from google.appengine.api import urlfetch
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.api import taskqueue

class BlobUploaded(blobstore_handlers.BlobstoreUploadHandler):
	def _authorize(self):
		self.uploaded_files = self.get_uploads()
		key = self.request.get("key", None)
		i("")
		i("\t uploaded files: " + str(len(self.uploaded_files)))
		i("\t\tKEY: " + str(key))
		i("")
		if key is None:
			self.abort(200, "must provide key param", body_template='{"message": "IDENTITY ERROR", "error_code": 8, "result": false}')
			return
		query = main.Credentials.query(main.Credentials.session_key == key)
		self.auth = query.get()
		if not self.auth: 
			for blobinfo in self.uploaded_files:
				blobinfo.delete()
			self.abort(200, "key not valid {0}".format(key), body_template='{"message": "IDENTITY ERROR", "error_code": 8, "result": false}')
			return

	def _get_info(self):
        #camera
		owner = self.auth.owner.get()
		if owner:
			self.key = owner.key
			self.camera = owner.name
			self.user = owner.owner
			user = main.Credentials.query(main.Credentials.owner == owner.owner).get()
			if user:
				self.username = user.name
			else:
				self.username = None
		else:
			self.key = None
			self.camera = None
			self.user = None
			self.username = None

class VideoUploaded(BlobUploaded):
	def post(self):
		self._authorize()
		self._get_info()
		video_blobinfo = self.get_uploads("video_data")
		video_blobinfo = video_blobinfo and video_blobinfo[0]
		image_blobinfo = self.get_uploads("thumbnail_data")
		image_blobinfo = image_blobinfo and image_blobinfo[0]
		
		segment = main.VideoSegment()
		segment.owner = self.key
		segment.camera = self.camera
		segment.username = self.username
		segment.user = self.user
		segment.duration = float(self.request.get("duration", 0))
		if segment.duration == 0.0:
			self.response.write(dumps({"result" : False, "message" : "ZERO LENGTH VIDEO", "error_code" : 17}))
			e("")
			e("Aborting upload of zero length video {}".format(repr(self.response)))
			e("")
			return

		if self.user:
			if video_blobinfo:
				taskqueue.add(url='/dashboard/users/media_increment?key=%s&size=%s' % (self.user.urlsafe(), video_blobinfo.size), method='GET')
		segment.utc_start = datetime.fromtimestamp(float(self.request.get("utc_start", 0))) 
		segment.chkin_version = self.request.get("chkin_version", None)
		segment.video_width = self.request.get("video_width", None)
		segment.video_height = self.request.get("video_height", None)
		segment.title = self.request.get("title", None)
		segment.description = self.request.get("description", None)
		segment.video_name = self.request.get("video_name", None)
		if video_blobinfo:
			segment.video_data_blobkey = video_blobinfo.key()  #video_data.fullvalue()
			segment.size = video_blobinfo.size
		if image_blobinfo:
			segment.thumbnail_blobkey = image_blobinfo.key() #thumbnail.fullvalue()
		segment.thumbnail_width = self.request.get("thumbnail_width", None)
		segment.thumbnail_height = self.request.get("thumbnail_height", None)
		segment.put()

		self.response.write(dumps({"result" : True, "message" : "OK", "error_code" : 0}))

	# def get(self):
	# 	import pdb
	# 	pdb.set_trace()


# class ImageUploaded(blobstore_handlers.BlobstoreUploadHandler):
# 	def post(self):
# 		uploaded_files = self.get_uploads()
# 		import pdb
# 		pdb.set_trace()
# 		self.response.write('Hello, World!')

# 	def get(self):
# 		import pdb
# 		pdb.set_trace()

class APKUploaded(BlobUploaded):
	def post(self):
		self._authorize()
		apk_upload = self.get_uploads('apk')
		dump_upload = self.get_uploads('dump')
		seeds_upload = self.get_uploads('seeds')
		mapping_upload = self.get_uploads('mapping')
		usage_upload = self.get_uploads('usage')
		apk = main.ChkinAPK()
		apk.version = self.request.get("chkin_version", None);
		apk.build_id = self.request.get("build_id", "");
		if (apk.version == None) or (not check_version_format(apk.version)):
			self.abort(200, "must provide chkin_version feld", body_template='{"message": "NOT APPLICABLE", "error_code": 3, "result": false}')
			return;
		apk.apk_blobkey = apk_upload[0].key()
		if not (mapping_upload == None):
			apk.mapping_blobkey = mapping_upload[0].key()
		if not (usage_upload == None):
			apk.usage_blobkey = usage_upload[0].key()
		if not (dump_upload == None):
			apk.dump_blobkey = dump_upload[0].key()
		if not (seeds_upload == None):
			apk.seeds_blobkey = seeds_upload[0].key()
		apk.uploaded = datetime.now()
		apk.put()
		self.response.write(dumps({"result" : True, "message" : "OK", "error_code" : 0}))


def check_version_format(text):
	if text == None:
		return False
	## TODO check if this text is in valid version format
    ## meaning ii.ii.ii or someting like that 
	return True



app = webapp2.WSGIApplication([
    ('/push_video_blobed', VideoUploaded),
    ('/push_apk_blobed', APKUploaded),
    # ('/push_image_blobed', ImageUploaded),
], debug=True)
