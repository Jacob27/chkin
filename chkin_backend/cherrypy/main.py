#!/usr/bin/env python

import webapp2
import pdb

import os
import md5
import pytz
import random
import base64
import cherrypy
import threading
import time
import ConfigParser
import calendar
import traceback
import pygeoip
from math import floor
from json import dumps, loads
from uuid import uuid4
from datetime import datetime, timedelta
from logging import debug as d, info as i, warning as w, error as e, critical as c
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from validate_email import validate_email
from requests import get

# from gae_python_gcm.gcm import GCMMessage, GCMConnection

from paypal import PayPalInterface, PayPalConfig

Base = declarative_base()

"""
Authentication CHAP:
	- receive challange from server
	- MD5(password+challange) to server
	- Validate and return session_key to client
	session_key is used as passport for other actions


ovo su najbitnija polja za koja client i kamera koriste za komunikaciju
{
"recording_quality" : "LD",
"night_vision" : "false",
"motion_sensor" : "false",
"microphone" : "false",
"speaker" : "false",
"external_ip" : "",
"external_port" : "-1",
"internal_ip" : "",
"internal_port" : "9999"
}

Date and time conversions:
	timestamp = (dt - datetime(1970, 1, 1)).total_seconds()

	dt = datetime.fromtimestamp(timestamp, tz=pytz.utc)

Test:

$ curl "http://127.0.0.1:8080/register_device?name=a&password=b"
{"message": "OK", "error_code": 0, "result": true}
$ curl "http://127.0.0.1:8080/register_user?name=alexandarkordic@gmail.com&password=alex"
{"message": "OK", "error_code": 0, "result": true}
$ curl "http://127.0.0.1:8080/auth?name=a&response=7a35e142518436f1d8d51869ff5d31fc"
{"message": "OK", "key": "c231ff25-b94c-4ad2-bd39-41f0f7c7842b", "error_code": 0, "result": true}
$ curl "http://127.0.0.1:8080/claim_device_request?user_email_address=alexandarkordic@gmail.com&key=c231ff25-b94c-4ad2-bd39-41f0f7c7842b"
{"message": "OK", "result": true, "error_code": 0}
$ curl "http://127.0.0.1:8080/claim_device?id=NWE4OTJjMzktNzdjZi00OTI4LWIwOTQtY2I2MTk2OGI5NWE0&owner=YWxleGFuZGFya29yZGljQGdtYWlsLmNvbQ=="
{"error_code": 0, "result": true, "message": "OK"}
curl -F "key=c231ff25-b94c-4ad2-bd39-41f0f7c7842b" -F "utc_start=1403697771.0" -F "duration=5.99" -F "chkin_version=0.0.13" -F "video_width=900" -F "video_height=720" -F "title=earth" -F "description=globe" -F "video_name=test" -F "video_data=@c:/forge/work/chkin/gae/chkin/testdata/output_50.ts" -F "thumbnail=@c:/forge/work/chkin/gae/chkin/testdata/output_50.ts.jpg" -F "thumbnail_width=450" -F "thumbnail_height=360" "http://127.0.0.1:8080/push_video"

1403697771.0

"""

# # Geo IP sqlite DB
gic = pygeoip.GeoIP("./GeoLiteCity.dat", flags=pygeoip.const.MEMORY_CACHE)

Config = ConfigParser.ConfigParser()
Config.read("./config.ini")

SERVER_HOSTNAME = Config.get("server", "hostname")
SERVER_PORT = Config.get("server", "port")
# # in seconds
SESSION_KEY_ROTATION_TIME_INTERVAL = int(Config.get("server", "key_rotation_interval"))

SERVER_SENDER_ADDRESS = "ChkInCam Support <chkincam@gmail.com>"
SUBSCRIPTION_COST = 10

# # in seconds
HARTBEAT_TIME_INTERVAL = 10 * 60

HARTBEAT_PARAMETER_NAME = "last_hartbeat_at"
ONLINE_PARAMETER_NAME = "online"
GCM_ACTION_KEY_PASSWORD_CHANGED = "app.chkin.PASSWORD_CHANGED"
GCM_PASSWORD_KEY = "pw"
GCM_PASSWORD_CHANGED_COLLAPSE_KEY = "pw_changed"

## In minutes
NOTIFICATION_INTERVAL = 30

# google recaptcha secret key
# must be kept secret
CAPTCHA_SECRET = "6LfKRAETAAAAAIHgUCv_pZLPf9GGKDbJZIG7yVvc"

CAMERA_DEFAULT_VALUES = {
    "recording_quality": "HD",
    "night_vision": "false",
    "motion_sensor": "false",
    "microphone": "true",
    "speaker": "false",
    "external_ip": "-1",
    "internal_ip": "-1",
    "external_port": "-1",
    "internal_port": "-1",
    "recording_mode": "false",
    "manual_port": "-1",
    "email_alert_active": "false",
    "motion_event_alert_email": "false",
    "motion_event_alert_sms": "false",
    "loud_noise_alert_email": "false",
    "loud_noise_alert_sms": "false",
    "camera_offline_alert_sms": "false",
    "camera_offline_alert_email": "false",
    "schedule_active": "false",
    "schedule_by_days": "None",
    "schedule_from": "None",
    "schedule_to": "None",
    "subscription": "false",
    "gcm_push_token": "",
    "device_title": "",
    "camera_timezone_time_diff": "0",
    "wakeup_auto_monitor": "false",
    "last_hartbeat_at": "-1",
    "online": "false",
    "chkin_version": "1.0.0",
    "camera_timezone_local_area": "",
    "camera_offline_alert": "false",
    "motion_event_alert": "false",
    "loud_noise_alert": "false",
    "camera_timezone": "",
    "camera_timezone_string": "",
    "sms_alert_active": "false",
    "email_alert": "false",
    "device_address": "",
}


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    # contact information
    # cameras = ndb.KeyProperty(kind=Camera, repeated=True)
    subscription = ('subscription', Integer, ForeignKey("subscription.id"))
    subscription_start_time = Column(DateTime)
    subscription_end_time = Column(DateTime)
    state = ('parameter', Integer, ForeignKey("parameter.id"))
    name = Column(String)
    phone_number = Column(String)
    telephone_co = Column(String)
    camera_count = Column(Integer)
    media_count = Column(Integer)
    storage_used = Column(Integer)
    deleted = Column(Boolean)
    usage = ('usage', Integer, ForeignKey("usage.id"))
    registered_on = Column(DateTime)


class PaymentRecord(Base):
    __tablename__ = 'payment_record'

    id = Column(Integer, primary_key=True)
    owner = ('user', Integer, ForeignKey("user.id"))
    token = Column(String)
    start = Column(DateTime)
    end = Column(DateTime)
    price = Column(Float)
    plan_type = Column(String)
    plan_days = Column(Integer)
    # checkout_setup_response = ndb.JsonProperty()
    # transaction_details_response = ndb.JsonProperty()
    # execution_response = ndb.JsonProperty()


class Credentials(Base):
    __tablename__ = 'credentials'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    password = Column(String)
    challange = Column(String)
    session_key = Column(String)
    session_created = Column(DateTime)
    # type = Column(String)
    # user = ndb.KeyProperty(kind="User")
    # camera = ndb.KeyProperty(kind="Camera")
    owner = ('user', Integer, ForeignKey("user.id"))


class ClientSession(Base):
    __tablename__ = 'client_session'

    id = Column(Integer, primary_key=True)
    client = Column(String)
    session_key = Column(String)
    expires_at = Column(DateTime)
    ip_address = Column(String)
    logged_in = Column(DateTime)
    logged_out = Column(DateTime)
    owner = ('user', Integer, ForeignKey("user.id"))


class Parameter(Base):
    __tablename__ = 'parameter'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    value = Column(String)


"""
class Feature(Base):
	features = ndb.KeyProperty(kind=Feature, repeated=True)
"""


class Packet(Base):
    __tablename__ = 'packet'

    id = Column(Integer, primary_key=True)
    name = Column(String)


class Subscription(Base):
    __tablename__ = 'subscription'

    id = Column(Integer, primary_key=True)
    packets = ('packet', Integer, ForeignKey("packet.id"))


class VideoSegment(Base):
    __tablename__ = 'video_segment'

    id = Column(Integer, primary_key=True)
    owner = ('user', Integer, ForeignKey("user.id"))
    utc_start = Column(DateTime)
    duration = Column(Float)
    chkin_version = Column(String)
    video_width = Column(String)
    video_height = Column(String)
    title = Column(String)
    description = Column(String)
    video_name = Column(String)
    # video_data_blobkey = ndb.BlobKeyProperty()
    # thumbnail_blobkey = ndb.BlobKeyProperty()
    thumbnail_width = Column(String)
    thumbnail_height = Column(String)
    camera = Column(String)
    username = Column(String)
    user = ('user', Integer, ForeignKey("user.id"))
    size = Column(Integer)
    deleted = Column(Boolean)


class Statistics(Base):
    __tablename__ = 'statistics'

    id = Column(Integer, primary_key=True)
    daily = Column(Integer)
    weekly = Column(Integer)
    monthly = Column(Integer)

    def json(self):
        return {'daily': self.daily, 'weekly': self.weekly, 'monthly': self.monthly}


class Usage(Base):
    __tablename__ = 'usage'

    id = Column(Integer, primary_key=True)
    storage = ('statistics', Integer, ForeignKey("statistics.id"))
    downloaded = ('statistics', Integer, ForeignKey("statistics.id"))

    def json(self):
        return {'storage': self.storage.json(), 'downloaded': self.downloaded.json()}


class Camera(Base):
    __tablename__ = 'camera'

    id = Column(Integer, primary_key=True)
    owner = ('user', Integer, ForeignKey("user.id"))
    state = ('parameter', Integer, ForeignKey("parameter.id"))
    claim_token = Column(String)
    gcm_push_token = Column(String)
    build_id = Column(String)
    name = Column(String)
    user = Column(String)
    deleted = Column(Boolean)
    usage = ('usage', Integer, ForeignKey("usage.id"))
    creation_date = Column(DateTime, default=func.now())


class GCMCommand(Base):
    __tablename__ = 'gcm_command'

    id = Column(Integer, primary_key=True)
    camera = ('camera', Integer, ForeignKey("camera.id"))
    utc_expiry = Column(DateTime)


class ChkinAPK(Base):
    __tablename__ = 'chkin_apk'

    id = Column(Integer, primary_key=True)
    ## String, format : major.minor version
    version = Column(String)
    uploaded = Column(DateTime)
    # apk_blobkey = ndb.BlobKeyProperty()
    # dump_blobkey = ndb.BlobKeyProperty()
    # mapping_blobkey = ndb.BlobKeyProperty()
    # seeds_blobkey = ndb.BlobKeyProperty()
    # usage_blobkey = ndb.BlobKeyProperty()
    build_id = Column(String)


# this is not needed for now.
# class EventLog(Base):
# 	"""	store all the pending notifications for camera wise
#	if the notification can't be sent
# 	"""
# 	camera = ndb.KeyProperty(kind='Camera')
# 	event_type = Column(String) # same as notification type
# 	event_time = Column(DateTime)
# 	notified = ndb.BooleanProperty(default=False)

# 	def get_pending_records(self, camera, last_notification_timestamp):
# 		return self.query(self.camera == camera, 
# 			self.notified == False, 
# 			self.event_time > last_notification_timestamp).fetch()


class SmsEmailNotificationLog(Base):
    """log for sms and email notification """
    __tablename__ = 'sms_email_notification_log'

    id = Column(Integer, primary_key=True)
    camera_id = Column(String)
    alert_type = Column(String)
    timestamp = Column(DateTime, default=func.now())


class Authenticate(webapp2.RequestHandler):
    def get(self):
        self.response.write("2: " + str(self.request.params))

def datetime_to_seconds(date):
    return (date - datetime(1970, 1, 1)).total_seconds()


def check_credential_session_key_rotation(auth):
    "Return True in case new session key have been generated, False otheerwise"
    ## check if key rotation is needed
    session_created = auth.session_created
    ## current session start time in seconds
    current_session_started = datetime_to_seconds(session_created)
    current_time = datetime_to_seconds(datetime.now())
    time_diff = current_time - current_session_started
    i("")
    i("Session keys timedifference in seconds : " + str(time_diff) +
      " session key rotation time interval : " + str(SESSION_KEY_ROTATION_TIME_INTERVAL))
    i("")
    if (time_diff > SESSION_KEY_ROTATION_TIME_INTERVAL):
        i("")
        i(" Rotating session key for username : " + auth.name)
        i("")
        ## generate new session key
        auth.session_key = generate_session_key()
        auth.challange = generate_challange()
        auth.session_created = datetime.now()
        auth.put()
        return True
    else:
        return False


def generate_challange():
    return '%030x' % random.randrange(16 ** 30)


def generate_session_key():
    return str(uuid4())


def generate_claim_camera_token():
    return str(uuid4())


def date_to_timestamp(dt):
    if dt is None: return None
    return (dt - datetime(1970, 1, 1)).total_seconds()


def params_to_dict(list_of_params):
    ret = {}
    for param in list_of_params:
        if not (param.name in CAMERA_DEFAULT_VALUES.keys()):
            e("params_to_dict>>>> " + param.name + " not defined as a default camera value, skipping this field !!!")
            continue
        ret[param.name] = param.value
    return ret


def key_to_jsonparam(key):
    # return base64.urlsafe_b64encode(str(key))
    return key.urlsafe()


def key_from_jsonparam(param):
    # return ndb.Key(encoded=base64.urlsafe_b64decode(str(param)))
    return ndb.Key(urlsafe=param)

## TODO make code generator for python and java
ERROR_CODES = {
    "OK": 0,
    "NOT REGISTERED": 1,
    "BAD EMAIL": 2,
    "NOT APPLICABLE": 3,  # camera action performed from user
    "MISSING LINK": 4,  # comera key is missing !!!!
    "CAMERA ALREADY CLAIMED": 5,

    "CAMERA DELETED": 6,
    "USER DELETED": 7,

    "IDENTITY ERROR": 8,
    "CREDENTIALS MISSING": 9,
    "DUPLICATE": 10,
    "BROKEN LINK": 11,  # also in case you access foreign camera
    "MALFORMED_JSON": 12,
    "PAYMENT_CALCULATION_ISSUE": 13,
    "CAPTCHA VERIFICATION FAILED": 14,
    "SESSION NOT FOUND": 15,
    "NOT SUBSCRIBED": 16,
    "ZERO LENGTH VIDEO": 17,
}


def ok():
    return dumps({"result": True, "message": "OK", "error_code": 0})


def error(message):
    w(message)
    return dumps({"result": False, "message": message, "error_code": ERROR_CODES.get(message, -1)})


def serve_result(obj):
    obj.update({"result": True, "message": "OK", "error_code": 0})
    return dumps(obj)


## https://developers.google.com/appengine/docs/python/ndb/keyclass
# ONLY_KEY = "ROOT"
# def root_key():
# 	ndb.Key(ONLY_KEY)

def camera_from_auth(auth):
    pass


class ErrorConvention(Exception):
    def __init__(self, err_message):
        self.msg = err_message


class MethodWrapper(object):
    def __init__(self, target):
        self.__target = target

    def __getattr__(self, name):
        # w("MethodWrapper __getattr__({0})".format(name))
        return getattr(self.__target, name)

    def __call__(self, *a, **kw):
        # w("MethodWrapper __call__")
        try:
            cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"  # leave it for now
            return self.__target(*a, **kw)
        except ErrorConvention, e:
            return error(e.msg)


class AppWrapper(object):
    """ Used for catching exceptions and returning json error format instead of http protocol errors
    """

    def __init__(self, obj):
        # w("AppWrapper __init__")
        self.__obj = obj

    def __getattr__(self, name):
        # w("AppWrapper __getattr__({0})".format(name))
        ret = getattr(self.__obj, name)
        if callable(ret):
            return MethodWrapper(ret)
        else:
            return ret


class App(object):
    ## Need to get rid of error - catch exception in handler and convert to json response
    def _authorize(self):
        if cherrypy.request.cookie.has_key("key"):
            _key = cherrypy.request.cookie["key"].value
            d(" --- auth --- key={0}".format(_key))
        else:
            if cherrypy.request.params.has_key("key"):
                _key = cherrypy.request.params.get("key")
                d(" --- auth --- key={0}".format(_key))
            else:
                return self.error('CREDENTIALS MISSING')

        query = Credentials.query(Credentials.session_key == _key)
        auth = query.get()
        if not auth:
            ## all_credentials = Credentials.query().get()
            """
            print "\n\n " , "could not authorize key : " , _key + " DB returned : " , auth , \
                    " , all credentials : " , all_credentials , "\n\n"
            """
            return self.error('IDENTITY ERROR')
        if (auth.session_created is None):
            auth.session_created = datetime.now()
            auth.put()
            return auth
        if check_credential_session_key_rotation(auth):
            return self.error('IDENTITY ERROR')
        else:
            return auth

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("/frontend/login.html")

    # return 'Hello CherryPy!'

    @cherrypy.expose
    def chl(self, name):
        i("    name={0}".format(name))
        ## all names should be case insenssitive
        name = str(name).lower()
        query = Credentials.query(Credentials.name == name)
        auth = query.get()
        if not auth:
            return error("NOT REGISTERED")
        return serve_result({"chl": auth.challange})

    @cherrypy.expose
    def auth(self, name, response):
        ## all names should be case insenssitive
        name = str(name).lower()
        query = Credentials.query(Credentials.name == name)
        auth = query.get()
        if not auth:
            return error("NOT REGISTERED")
        raw = auth.password + auth.challange
        actual = md5.new(raw).hexdigest()
        i("sent={0} actual={1} ch={2} pas={3}, raw={4}".format(response, actual, auth.challange, auth.password, raw))
        if actual == response:
            if auth.session_key == None:
                auth.session_key = generate_session_key()
                auth.session_created = datetime.now()
                auth.put()
            else:
                check_credential_session_key_rotation(auth)
            return serve_result({"key": auth.session_key})
        else:
            return self.error('IDENTITY ERROR')

            # maybe we want to change challange now

    @cherrypy.expose
    def crash_report(self, subject="Crash Report", body="", **kwargs):
        attachments = []
        for key, val in kwargs.items():
            if hasattr(val, "fullvalue"):
                attachments.append((str(key), str(val.fullvalue())))
        mail.send_mail(
            SERVER_SENDER_ADDRESS,
            "tkordic.private@gmail.com",
            subject,
            body,
            attachments=attachments,
        )
        return ok()

    @cherrypy.expose
    def claim_device_request(self, user_email_address, **kwargs):
        auth = self._authorize()
        if not validate_email(user_email_address):
            return error("BAD EMAIL")
        # create
        if auth.owner == None:
            return error("MISSING LINK")
        camera = auth.owner.get()
        if not isinstance(camera, Camera):
            return error("NOT APPLICABLE")

        if camera.owner != None:
            user = camera.owner.get()
            # send the current user email
            subject = "Already claimed"
            body = """
The camera you are trying to claim is already claimed.
"""
            mail.send_mail(SERVER_SENDER_ADDRESS, user_email_address, subject, body)

            # send the original user
            subject = "Claim warning"
            body = """
Someone just tried to claim your camera.
"""
            mail.send_mail(SERVER_SENDER_ADDRESS, ouser.name, subject, body)
            return error("CAMERA ALREADY CLAIMED")

        # send email
        camera.claim_token = generate_claim_camera_token()
        camera.put()
        i("claim token generated {0}".format(camera.claim_token))
        confirmation_url = "http://{host}:{port}/claim_device?id={id}&owner={email}".format(
            id=base64.urlsafe_b64encode(camera.claim_token),
            host=SERVER_HOSTNAME,
            port=SERVER_PORT,
            email=base64.urlsafe_b64encode(user_email_address),
        )
        subject = "Confirm camera activation"
        body = """
Thank you for deploying new camera! Please confirm camera activation by
clicking on the link below:

{confirmation_url}
""".format(confirmation_url=confirmation_url)
        mail.send_mail(SERVER_SENDER_ADDRESS, user_email_address, subject, body)
        w("email body:")
        w(body)
        return ok()


    ### DO NOT FORGET <form action="/enter_credentials/" method="post"> ACTION ENDS WITH "/"
    @cherrypy.expose
    def enter_credentials(self, password_1, password_2, **kwargs):
        if password_1 != password_2:
            raise cherrypy.HTTPRedirect("/frontend/chose_pass.html")
        id = str(cherrypy.request.cookie["id"].value)
        email = str(cherrypy.request.cookie["owner"].value).lower()
        e("> > > id={0} email={1}".format(id, email))
        query = Camera.query(Camera.claim_token == id)
        camera = query.get()
        if camera is None:
            # TODO: redirect to error message page after few attempts because of eventual consistency
            # raise Exception("CAMERA DELETED")
            raise cherrypy.HTTPRedirect("/frontend/expired.html")
        query = Credentials.query(Credentials.name == email)
        auth = query.get()
        if auth is not None:
            raise cherrypy.HTTPRedirect("/frontend/expired.html")
        ## now we create user account
        user = User()
        user.name = name
        user.put()
        try:
            auth = Credentials()
            auth.name = email
            auth.password = password_1
            auth.challange = generate_challange()
            auth.owner = user.key
            auth.session_key = generate_session_key()
            auth.session_created = datetime.now()
            auth.put()
        except:
            user.key.delete()
            raise
        ## now login new user
        cherrypy.response.cookie["key"] = auth.session_key
        ## now we claim its device
        self._claim_device(auth, id, email, camera)

    def _claim_device(self, auth, id, email, camera):
        if auth.owner is None:
            # raise Exception("MISSING LINK")
            e("missing link for id={id} owner={o}".format(id=id, o=email))
            raise cherrypy.HTTPRedirect("/frontend/expired.html")  # bleffing: it is users mistake :o
        user = auth.owner.get()
        if user is None:
            # TODO: redirect to error message page after few attempts because of eventual consistency
            # raise Exception("USER DELETED")
            e("user deleted for id={id} owner={o}".format(id=id, o=email))
            raise cherrypy.HTTPRedirect("/frontend/expired.html")  # bleffing: it is users mistake :o
        if not isinstance(user, User):
            # TODO: redirect to error message page after few attempts because of eventual consistency
            # raise Exception("NOT APPLICABLE")
            e("not applicable for id={id} owner={o}".format(id=id, o=email))
            raise cherrypy.HTTPRedirect("/frontend/expired.html")  # bleffing: it is users mistake :o
        camera.owner = auth.owner
        user = auth.owner.get()
        if user:
            camera.user = user.name
            taskqueue.add(url='/dashboard/users/camera_increment?key=%s' % (auth.owner.urlsafe()), method='GET')

        camera.claim_token = None
        camera.put()
        # return ok()
        raise cherrypy.HTTPRedirect("/")  # continue to our site

    ## this is not API call so response shall not be JSON but error or success page redirection
    @cherrypy.expose
    def claim_device(self, id, owner, **kwargs):
        id = base64.urlsafe_b64decode(str(id))
        email = str(base64.urlsafe_b64decode(str(owner))).lower()
        query = Camera.query(Camera.claim_token == id)
        camera = query.get()
        if camera is None:
            # TODO: redirect to error message page after few attempts because of eventual consistency
            # raise Exception("CAMERA DELETED")
            e("CAMERA NOT FOUND claim token={0}".format(id))
            raise cherrypy.HTTPRedirect("/frontend/expired.html")
        query = Credentials.query(Credentials.name == email)
        auth = query.get()
        if auth is None:
            # raise Exception("Email not listed")
            cherrypy.response.cookie["id"] = id
            cherrypy.response.cookie["owner"] = email
            raise cherrypy.HTTPRedirect("/frontend/chose_pass.html")
        self._claim_device(auth, id, email, camera)

    # TODO: rename push_video into segment_upload_request
    @cherrypy.expose
    def segment_upload_request(self,
                               **kwargs):  #, utc_start, duration, chkin_version, video_width, video_height, title, description, video_name, thumbnail_width, thumbnail_height, **kwargs):
        auth = self._authorize()
        # TODO: check license here
        cam_user = auth.owner.get().owner.get()
        # cheking for the subscription
        if auth and cam_user:
            if not cam_user.subscription_end_time:
                return error("NOT SUBSCRIBED")
            is_subscribed = cam_user.subscription_end_time >= datetime.now()

            if is_subscribed:
                return serve_result(
                    {  # video_upload_url=blobstore.create_upload_url('/push_video_blobed?id={id}'.format(id=id)),
                       # image_upload_url=blobstore.create_upload_url('/push_image_blobed?id={id}'.format(id=id)),
                       "upload_url": blobstore.create_upload_url('/push_video_blobed'),
                       # "image_upload_url" : blobstore.create_upload_url('/push_image_blobed'),
                    })
            else:
                return error("NOT SUBSCRIBED")
        else:
            return ("IDENTITY ERROR")


    def _all_segments_(self, owner, from_time, to_time):
        segments = []
        videos = VideoSegment.query(VideoSegment.owner == owner, VideoSegment.utc_start >= from_time,
                                    VideoSegment.utc_start < to_time, VideoSegment.deleted == None).order(
            -VideoSegment.utc_start)
        for video_segment in videos:
            # do not send videos that has 0 length.
            # fixing corrupted video segments by checking size,
            # from the sample data, vidoes lengths start from 60s. and not lesser than 1 MB.
            if video_segment.duration != 0.0 and video_segment.size > 1000000:
                video_url = "http://{host}:{port}/video?id={id}".format(
                    id=base64.urlsafe_b64encode(str(video_segment.video_data_blobkey)),
                    host=SERVER_HOSTNAME,
                    port=SERVER_PORT,
                )
                image_url = "http://{host}:{port}/image?id={id}".format(
                    id=base64.urlsafe_b64encode(str(video_segment.thumbnail_blobkey)),
                    host=SERVER_HOSTNAME,
                    port=SERVER_PORT,
                )
                segments.append({  # "utc_start" : video_segment.utc_start,
                                   "utc_start": date_to_timestamp(video_segment.utc_start),
                                   "duration": video_segment.duration,
                                   "chkin_version": video_segment.chkin_version,
                                   "video_width": video_segment.video_width,
                                   "video_height": video_segment.video_height,
                                   "title": video_segment.title,
                                   "description": video_segment.description,
                                   "video_name": video_segment.video_name,
                                   "video_url": video_segment.video_data_blobkey and video_url,
                                   "thumbnail_url": video_segment.thumbnail_blobkey and image_url,
                                   "thumbnail_width": video_segment.thumbnail_width,
                                   "thumbnail_height": video_segment.thumbnail_height,
                })
        return segments

    def _camera_segments(self, camera_key, from_time, to_time):
        if from_time is None:
            from_time = datetime(1970, 1, 1)
        else:
            from_time = datetime.fromtimestamp(float(from_time))
        if to_time is None:
            to_time = datetime.now()
        else:
            to_time = datetime.fromtimestamp(float(to_time))
        segments = self._all_segments_(camera_key, from_time, to_time)
        return serve_result({"segments": segments})

    @cherrypy.expose
    def own_segments(self, from_time=None, to_time=None, **kwargs):
        "Returns segments of authenticated camera"
        auth = self._authorize()
        return self._camera_segments(auth.owner, from_time, to_time)

    @cherrypy.expose
    def camera_segments(self, id, from_time=None, to_time=None, **kwargs):
        "Returns segments of specified camera"
        auth = self._authorize()
        try:
            camera = key_from_jsonparam(id).get()
        except:
            return self.error("BROKEN LINK")
        if not isinstance(camera, Camera):
            return self.error("NOT APPLICABLE")
        # check if this is our camera
        if camera.owner != auth.owner:
            return self.error("BROKEN LINK")
        return self._camera_segments(camera.key, from_time, to_time)

    @cherrypy.expose
    def user_cameras(self, **kwargs):
        auth = self._authorize()
        user = auth.owner.get()
        if not isinstance(user, User):
            return self.error("NOT APPLICABLE")
        list_of_cameras = []
        cameras = Camera.query(Camera.owner == auth.owner).order(Camera.creation_date)
        for camera in cameras:
            cam_auth_query = Credentials.query(Credentials.owner == camera.key)
            cam_auth = cam_auth_query.get()
            if cam_auth == None:
                continue
            state = params_to_dict(camera.state)
            ## check if camera is online
            if state.has_key(HARTBEAT_PARAMETER_NAME):
                hartbeat_time = float(state[HARTBEAT_PARAMETER_NAME])
                if ( (time.time() - hartbeat_time) > HARTBEAT_TIME_INTERVAL ):
                    state[ONLINE_PARAMETER_NAME] = 'false'

            list_of_cameras.append({
                "state": state,
                "id": key_to_jsonparam(camera.key),
                "mac_address": cam_auth.name,  #"segments" : self._all_segments_(camera.key)
            })
        return serve_result({
            "cameras": list_of_cameras,
            "subscription_start_time": date_to_timestamp(user.subscription_start_time),
            "subscription_end_time": date_to_timestamp(user.subscription_end_time),
            "your_ip_address": cherrypy.request.remote.ip,
            "user_email": user.name,
            "phone_number": user.phone_number,
            "telephone_co": user.telephone_co
        })

    @cherrypy.expose
    def set_account_infos(self, key=None, json_state=None, **kwargs):
        auth = self._authorize()

        user = auth.owner.get()

        if json_state:
            kwargs = loads(json_state)
            e("account infos using json: {0}".format(repr(kwargs)))
            if not isinstance(kwargs, dict):
                return self.error("MALFORMED_JSON")
        else:
            e("account infos using kwargs: {0}".format(repr(kwargs)))

        for name, value in kwargs.items():
            e("_set_account_infos(camera, {0}, {1})".format(repr(name), repr(value)))
            if name == "phone_number":
                user.phone_number = value
            if name == "telephone_co":
                user.telephone_co = value

        user.put()
        return ok()


    def is_subscribed(self, user_key):
        """returns True if subscription available
        args:
        - `user_key`
        type:
        - `ndb.Key`
        entity:
        - `User`
        """
        user = user_key.get()
        if user.subscription_end_time:
            return user.subscription_end_time > datetime.now()
        return False


    @cherrypy.expose
    def set_camera_state(self, key=None, camera_id=None, json_state=None, send_gcm="true", **kwargs):
        e("Setting camera state for camera with id : {0}".format(repr(camera_id)))
        send_gcm = send_gcm == "true"
        auth = self._authorize()
        if camera_id is not None:
            camkey = key_from_jsonparam(camera_id)
            camera = camkey.get()
        else:
            camera = auth.owner.get()
            if not isinstance(camera, Camera):
                return self.error("NOT APPLICABLE")
        if json_state:
            kwargs = loads(json_state)
            e("setting state using json: {0}".format(repr(kwargs)))
            if not isinstance(kwargs, dict):
                return self.error("MALFORMED_JSON")
        else:
            e("setting state using kwargs: {0}".format(repr(kwargs)))

        gcm_payload, gcm_collapsekey = None, None
        sch_gcm_payload, sch_gcm_collapsekey = None, None

        # do not update from, to values if schedule is inactive
        if kwargs.get('schedule_active') == "false":
            kwargs.pop('schedule_from', None)
            kwargs.pop('schedule_to', None)

        if isinstance(auth.owner.get(), Camera):
            i("Set a timezon for camera by ip !!!")
            timezone = None
            try:
                timezone = gic.time_zone_by_addr(cherrypy.request.remote.ip)
            except Exception, ex:
                e("Error determening timezone for ip : " + cherrypy.request.remote.ip)
            if not (timezone is None):
                i("Camera new timezone is : " + timezone)
                kwargs["camera_timezone"] = timezone

        for name, value in kwargs.items():
            e("_set_camera_state(camera, {0}, {1})".format(repr(name), repr(value)))
            if name == "recording_mode":
                # detect if change in recording mode happened:
                old = self._get_camera_state_value(camera, "recording_mode")
                if send_gcm and (old is None or old != value):
                    # send gc command
                    w("[recording_mode changed] old={0} new={1}".format(old, value))
                    if value == "true":
                        # payload = {"action":"app.chkin.START_RECORDING_MODE"}, collapse_key = start_recording
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.START_RECORDING_MODE"}', 'start_recording'
                    else:
                        # payload = {"action":"app.chkin.STOP_RECORDING_MODE"}, collapse_key = stop_recording
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.STOP_RECORDING_MODE"}', 'stop_recording'
                self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)
            if name == "gcm_push_token":
                ## Update camera GCM push token
                i("Updating camera GCM push token from " + camera.gcm_push_token + " , to : " + value)
                camera.gcm_push_token = value
            if name == "microphone":
                if value == "true":
                    gcm_payload, gcm_collapsekey = '{"action":"app.chkin.MICROPHONE_ON"}', 'microphone_on'
                else:
                    gcm_payload, gcm_collapsekey = '{"action":"app.chkin.MICROPHONE_OFF"}', 'microphone_off'
                self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)

            if name == "email_alert_active":
                old = self._get_camera_state_value(camera, "email_alert_active")
                if (old is None or old != value):
                    if value == "true":
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT_ON"}', 'emailalert_on'
                    else:
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT_OFF"}', 'emailalert_off'
                    self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)

            if name == "motion_event_alert_email":
                old = self._get_camera_state_value(camera, "motion_event_alert_email")
                if (old is None or old != value):
                    if value == "true":
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "motion_event_alert_email":"true"}', 'motion_event_alert_email_on'
                    else:
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "motion_event_alert_email":"false"}', 'motion_event_alert_email_off'
                    self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)

            if name == "loud_noise_alert_email":
                old = self._get_camera_state_value(camera, "loud_noise_alert_email")
                if (old is None or old != value):
                    if value == "true":
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "loud_noise_alert_email":"true"}', 'loud_noise_alert_email_on'
                    else:
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "loud_noise_alert_email":"false"}', 'loud_noise_alert_email_off'
                    self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)

            if name == "motion_event_alert_sms":
                old = self._get_camera_state_value(camera, "motion_event_alert_sms")
                if (old is None or old != value):
                    if value == "true":
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "motion_event_alert_sms":"true"}', 'motion_event_alert_sms_on'
                    else:
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "motion_event_alert_sms":"false"}', 'motion_event_alert_sms_off'
                    self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)

            if name == "loud_noise_alert_sms":
                old = self._get_camera_state_value(camera, "loud_noise_alert_sms")
                if (old is None or old != value):
                    if value == "true":
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "loud_noise_alert_sms":"true"}', 'loud_noise_alert_sms_on'
                    else:
                        gcm_payload, gcm_collapsekey = '{"action":"app.chkin.EMAILALERT", "loud_noise_alert_sms":"false"}', 'loud_noise_alert_sms_off'
                    self._send_gcm_message(camera, gcm_payload, gcm_collapsekey)

            if name == "schedule_active":
                if value == "true":
                    sch_gcm_collapsekey = 'schedule_on'
                    if sch_gcm_payload is None:
                        sch_gcm_payload = '"action":"app.chkin.SCHEDULE_ON"'
                    else:

                        sch_gcm_payload = sch_gcm_payload + ',"action":"app.chkin.SCHEDULE_ON"'

                else:
                    sch_gcm_collapsekey = 'schedule_off'
                    if sch_gcm_payload is None:
                        sch_gcm_payload = '"action":"app.chkin.SCHEDULE_OFF"'
                    else:
                        sch_gcm_payload = sch_gcm_payload + ',"action":"app.chkin.SCHEDULE_OFF"'

            if name == "schedule_by_days":
                if sch_gcm_payload is None:
                    sch_gcm_payload = '"schedule_by_days":"' + value + '"'
                else:
                    sch_gcm_payload = sch_gcm_payload + ',"schedule_by_days":"' + value + '"'

            if name == "schedule_from":
                if sch_gcm_payload is None:
                    sch_gcm_payload = '"schedule_from":"' + value + '"'
                else:
                    sch_gcm_payload = sch_gcm_payload + ',"schedule_from":"' + value + '"'

            if name == "schedule_to":
                if sch_gcm_payload is None:
                    sch_gcm_payload = '"schedule_to":"' + value + '"'
                else:
                    sch_gcm_payload = sch_gcm_payload + ',"schedule_to":"' + value + '"'

            # if name == "wakeup_auto_monitor":
            # 	if value == "true":
            # 		if not self.is_subscribed(camera.owner):(camera.owner.is_subscribed)
            # 			value = "false"
            # 			i("{0} doesn't have any subscription, Auto monitor is turned OFF".format(camera.user))

            # 	TODO: get android API commands
            # 	if sch_gcm_payload is None:
            # 		sch_gcm_payload = '"wakeup_auto_monitor":"' + value + '"'
            # 	else:
            # 		sch_gcm_payload = sch_gcm_payload + ',"wakeup_auto_monitor":"' + value + '"'

            self._set_camera_state(camera, name, value)
            e("state after set: {0}".format(repr(camera.state)))

        if kwargs.get('schedule_active') == "false":
            # reset the database values id `schedule_active` is inactive
            camera.state = [item for item in camera.state if item.name not in ['schedule_to', 'schedule_from']]
            camera.put()

        e("sch_gcm_payload: {0}".format(repr(sch_gcm_payload)))
        if sch_gcm_payload is not None:
            sch_gcm_payload = "{" + sch_gcm_payload + "}"
            self._send_gcm_message(camera, sch_gcm_payload, sch_gcm_collapsekey)

        camera.put()
        return ok()


    # def wakeup_auto_monitor(self):
    # 	"""
    # 	cron job to set the auto monitor on camera wakeup
    # 	from scheduler
    # 	XXX: As of now API does not `start_record_at` , `stop_record_at` something like that.
    # 	If there we can not map the scheduler time to recording scheduler.
    # 	XXX: currently, we do it manually checking it at 1 min interval.
    # 	"""
    # 	cameras = Camera.query():
    # 	for camera in cameras:


    @cherrypy.expose
    def test_sms(self, key=None, json_state=None, **kwargs):
        auth = self._authorize()

        user = auth.owner.get()

        if json_state:
            kwargs = loads(json_state)
            e("account infos using json: {0}".format(repr(kwargs)))
            if not isinstance(kwargs, dict):
                return self.error("MALFORMED_JSON")
        else:
            e("account infos using kwargs: {0}".format(repr(kwargs)))

        phone_number = ""
        telephone_co = ""
        for name, value in kwargs.items():
            e("_set_account_infos(camera, {0}, {1})".format(repr(name), repr(value)))
            if name == "phone_number":
                phone_number = value
            if name == "telephone_co":
                telephone_co = value

        subject = "Test SMS"
        body = "This Message is sent for test SMS."
        new_receiver = phone_number + telephone_co
        w(phone_number)
        w(telephone_co)
        w(new_receiver)
        #mail.send_mail(SERVER_SENDER_ADDRESS, new_receiver, subject, body)
        message = mail.EmailMessage(sender=SERVER_SENDER_ADDRESS, subject=subject)
        message.to = new_receiver
        message.cc = "seaman9297@gmail.com"
        message.body = body
        message.send()

        return ok()

    @cherrypy.expose
    def test_mail(self):
        self._send_email_sms_alert("offline_alert_email", "seaman9297@gmail.com", "13480862321", "sfsd32223", "dog",
                                   "ddd232121", " ", "America/Los_Angeles")
        return ok()

    def sms_mail_quota_check(self, camera_key, alert_type):
        """
        looks for the notification of same kind in under 30 mins.
        if not, returns True
        """
        try:
            log = SmsEmailNotificationLog.query(
                SmsEmailNotificationLog.camera_id == str(camera_key.id()),
                SmsEmailNotificationLog.alert_type == alert_type).get()

            if log:
                if log.timestamp + timedelta(0, NOTIFICATION_INTERVAL * 60) <= datetime.now():
                    i("The time difference is more than {0} min.".format(NOTIFICATION_INTERVAL))
                    log.timestamp = datetime.now()
                    log.put()
                    return True  # alert can be sent
                else:
                    i("The time difference is less than {0} min.".format(NOTIFICATION_INTERVAL))
                    return False  # alert can not be sent
            else:
                log = SmsEmailNotificationLog()
                log.populate(camera_id=str(camera_key.id()), alert_type=alert_type)
                log.put()
                return True  # alert can be sent

        except Exception as e:
            i("Exception in time difference calculation, Sending the notification Anyway")
            i(e)
            return True  # alert can be sent


    def _send_email_sms_alert(self, alert_type, receiver, phone_number, telephone_co, camera_name, mac_address,
                              camera_key, camera_timezone):

        subject = ""
        body = ""
        html = ""
        email_alert = False
        sms_alert = False

        if alert_type == "motion_event_alert_email":
            if self.sms_mail_quota_check(camera_key, alert_type):
                email_alert = True
                subject = "Motion Alert"
                videos = VideoSegment.query(VideoSegment.owner == camera_key, VideoSegment.deleted == None).order(
                    -VideoSegment.utc_start)
                video_segment = None
                if videos is not None:
                    video_segment = videos.get()
                time_str = datetime.now(pytz.timezone(camera_timezone)).strftime('%I:%M:%S %p on %A %B %d')
                if video_segment != None:
                    img_url = "image?id={id}".format(id=base64.urlsafe_b64encode(str(video_segment.thumbnail_blobkey)))
                else:
                    img_url = ""
                html = """<html><body><div><font color="#000000"><b>Chk-In Cam detected motion event on {camera_name} at {motion_time}. </b></font><br><br><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">Here is a snapshot at {motion_time}. <br><div style="width:500px;line-height:140%;display:inline-block"><br></div><a href="http://{host}:{port}" target="_blank"><img alt="Activity Detected!" src="http://{host}:{port}/{img_url}" border="0"></a> <br><br>CVR Subscribers: Click on the image to connect to ChkInCam and view the recorded footage for any unusual activities.</div></div><div><font color="#666666"><br></font></div><div><div style="color:rgb(102,102,102);font-size:13px;width:580px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">You are receiving this email because you asked to be notified when your camera detects activities. You can change your notification&nbsp;<a href="http://{host}:{port}" style="text-decoration:none" target="_blank">settings</a>&nbsp;by logging in to your chkincam account.</div><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">&copy; Chk-In&nbsp;</div><span style="color:rgb(102,102,102)">&nbsp;</span><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:80px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">Have comments or questions? Email&nbsp;<a href="http://{host}:{port}/support" style="text-decoration:none" target="_blank">Chk-In Support</a>.&nbsp;<br><br><a href="http://{host}:{port}/privacy" style="text-decoration:none" target="_blank">Privacy Policy</a>&nbsp;|&nbsp;<a href="http://{host}:{port}/TOS" style="text-decoration:none" target="_blank">Terms of Service</a></div><font color="#666666"><br></font></div></body></html>""".format(
                    camera_name=camera_name, motion_time=time_str, host=SERVER_HOSTNAME, port=SERVER_PORT,
                    img_url=img_url)
            else:
                i("Already a notification has been sent under {0} mins".format(NOTIFICATION_INTERVAL))

        if alert_type == "loud_noise_alert_email":
            if self.sms_mail_quota_check(camera_key, alert_type):
                email_alert = True
                subject = "Noise Alert"
                videos = VideoSegment.query(VideoSegment.owner == camera_key, VideoSegment.deleted == None).order(
                    -VideoSegment.utc_start)
                video_segment = None
                if videos is not None:
                    video_segment = videos.get()
                time_str = datetime.now(pytz.timezone(camera_timezone)).strftime('%I:%M:%S %p on %A %B %d')
                if video_segment != None:
                    img_url = "image?id={id}".format(id=base64.urlsafe_b64encode(str(video_segment.thumbnail_blobkey)))
                else:
                    img_url = ""
                html = """<html><body><div><b style="color:rgb(0,0,0)">Chk-In Cam detected loud noise on {camera_name} at {noise_time}.&nbsp;</b><br></div><div><br><div style="width:500px;color:rgb(102,102,102);line-height:21px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">Here is a snapshot at {noise_time}.&nbsp;<br><div style="width:500px;display:inline-block"><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#666666">&nbsp;For Motion Event</font></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#666666"><br></font></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#000000"><b>Chk-In Cam detected loud noise event on {camera_name} at {noise_time}.&nbsp;</b></font><br><br><div style="width:500px;color:rgb(102,102,102);line-height:21px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">Here is a snapshot at 7:09:41 AM on Thursday, December 18.&nbsp;</div></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><div style="width:500px;color:rgb(102,102,102);line-height:21px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block"><div style="width:500px;display:inline-block"><br></div><a href="http://{host}:{port}" target="_blank"><img alt="Activity Detected!" src="http://{host}:{port}/{img_url}" border="0"></a><br><br>CVR Subscribers: Click on the image to connect to ChkInCam and view the recorded footage for any unusual activities.</div></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#666666"><br></font></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><div style="color:rgb(102,102,102);font-size:13px;width:580px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">You are receiving this email because you asked to be notified when your camera detects activities. You can change your notification&nbsp;<a href="http://{host}:{port}/" style="text-decoration:none" target="_blank">settings</a>&nbsp;by logging in to your chkincam account.</div><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">&copy; Chk-In&nbsp;</div><span style="color:rgb(102,102,102)">&nbsp;</span><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:80px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">Have comments or questions? Email&nbsp;<a href="http://{host}:{port}/support" style="text-decoration:none" target="_blank">Chk-In Support</a>.&nbsp;<br><br><a href="http://{host}:{port}/privacy" style="text-decoration:none" target="_blank">Privacy Policy</a>&nbsp;|&nbsp;<a href="http://{host}:{port}/TOS" style="text-decoration:none" target="_blank">Terms of Service</a></div></div></div><br></div></div></body></html>""".format(
                    camera_name=camera_name, noise_time=time_str, host=SERVER_HOSTNAME, port=SERVER_PORT,
                    img_url=img_url)
            else:
                i("Already a notification has been sent under {0} mins".format(NOTIFICATION_INTERVAL))

        if alert_type == "offline_alert_email":
            if self.sms_mail_quota_check(camera_key, alert_type):
                email_alert = True
                subject = "Offline Alert"
                time_str = datetime.now(pytz.timezone(camera_timezone)).strftime('%I:%M:%S %p on %A %B %d')
                html = """<html><body><div>"{camera_name}" is offline. <br><br><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">"{camera_name}" &nbsp;went offline at {offline_time}.  <br><br>The lost of connection may be the result of power outage, Internet connection interruption, or someone disconnect the camera.</div></div><div><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block"><br></div></div><div><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">If you subscribes to the CVR service, please connect to Chk-In cam to review earlier footage for unusual activities.</div></div><div><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block"><br></div></div><div><font color="#666666"><div style="width:580px;line-height:120%;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:13px;word-spacing:1px;display:inline-block">You are receiving this email because you asked to be notified when your camera goes offline. You can change your notification <a style="text-decoration:none" href="http://{host}:{port}" target="_blank">settings</a> by logging in to your chkincam account.</div><br><br><div style="line-height:120%;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:13px;word-spacing:1px;display:inline-block">&copy; Chk-In&nbsp;</div>&nbsp;<br><br><div style="line-height:120%;padding-right:80px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:13px;word-spacing:1px;display:inline-block">Have comments or questions? Email <a style="text-decoration:none" href="http://{host}:{port}/support" target="_blank">Chk-In Support</a>. <br><br><a style="text-decoration:none" href="http://{host}:{port}/privacy" target="_blank">Privacy Policy</a> | <a style="text-decoration:none" href="http://{host}:{port}/TOS" target="_blank">Terms of Service</a></div></font><br></div></body></html>""".format(
                    camera_name=camera_name, offline_time=time_str, host=SERVER_HOSTNAME, port=SERVER_PORT)
            else:
                i("Already a notification has been sent under {0} mins".format(NOTIFICATION_INTERVAL))

        if alert_type == "motion_event_alert_sms":
            if self.sms_mail_quota_check(camera_key, alert_type):
                sms_alert = True
                subject = "Motion Alert"
            else:
                i("Already a notification has been sent under {0} mins".format(NOTIFICATION_INTERVAL))

        if alert_type == "loud_noise_alert_sms":
            if self.sms_mail_quota_check(camera_key, alert_type):
                sms_alert = True
                subject = "Noise Alert"
            else:
                i("Already a notification has been sent under {0} mins".format(NOTIFICATION_INTERVAL))

        if alert_type == "offline_alert_sms":
            if self.sms_mail_quota_check(camera_key, alert_type):
                sms_alert = True
                subject = "Offline Alert"
                body = """Your camera "{camera_name}"({mac_address}) got offline.""".format(camera_name=camera_name,
                                                                                            mac_address=mac_address)
            else:
                i("Already a notification has been sent under {0} mins".format(NOTIFICATION_INTERVAL))

        if (email_alert or sms_alert) and subject == "":
            e("alert type not reckognized, returning : NOT APPLICABLE")
            return self.error("NOT APPLICABLE")

        if email_alert == True:
            try:
                e("Sending email with subject : {0} , receiver : {1}".format(repr(subject), repr(receiver)))
                message = mail.EmailMessage(sender=SERVER_SENDER_ADDRESS, subject=subject)
                message.to = receiver
                message.html = html
                message.send()
            except:
                ## Verry dangerous to skip the exception
                e("Exception raised !!!")
                traceback.print_exc()

        if sms_alert == True:
            if phone_number and telephone_co:
                e("sms_alert phone: {0}".format(repr(phone_number)))
                e("sms_alert telephone_co: {0}".format(repr(telephone_co)))
                new_receiver = phone_number + telephone_co
                e("sms_alert new_receiver: {0}".format(repr(new_receiver)))
                #mail.send_mail(SERVER_SENDER_ADDRESS, new_receiver, subject, body)
                message = mail.EmailMessage(sender=SERVER_SENDER_ADDRESS, subject=subject)
                message.to = new_receiver
                message.cc = "seaman9297@gmail.com"
                message.body = body
                message.send()

        return ok()

    def send_sms(self):
        return ok()

    @cherrypy.expose
    def send_email_sms_alert(self, key=None, camera_id=None, json_state=None, **kwargs):
        auth = self._authorize()
        if camera_id is not None:
            camkey = key_from_jsonparam(camera_id)
            camera = camkey.get()
        else:
            camera = auth.owner.get()
            if not isinstance(camera, Camera):
                return self.error("NOT APPLICABLE")

        if json_state:
            kwargs = loads(json_state)
            e("email alert using json: {0}".format(repr(kwargs)))
            if not isinstance(kwargs, dict):
                return self.error("MALFORMED_JSON")
        else:
            e("email alert using kwargs: {0}".format(repr(kwargs)))

        for name, value in kwargs.items():
            e("_email_alert({0}, {1})".format(repr(name), repr(value)))

            if value == "true":
                state = params_to_dict(camera.state)
                camera_name = ""
                if state.has_key("device_title") and state["device_title"]:
                    camera_name = state["device_title"]
                cam_auth_query = Credentials.query(Credentials.owner == camera.key)
                cam_auth = cam_auth_query.get()
                if cam_auth == None:
                    continue
                mac_address = cam_auth.name
                if camera.owner == None:
                    return self.error("NOT APPLICABLE")
                cuser = camera.owner.get()
                receiver = cuser.name
                phone_number = cuser.phone_number
                telephone_co = cuser.telephone_co
                timezone_str = ""
                if state.has_key("camera_timezone_local_area") and state["camera_timezone_local_area"]:
                    timezone_str = state["camera_timezone_local_area"]
                self._send_email_sms_alert(name, receiver, phone_number, telephone_co, camera_name, mac_address,
                                           camera.key, timezone_str)
            e("send email alert after set: {0}".format(repr(name)))

        return ok()

    @cherrypy.expose
    def check_camera_offline(self):

        cameras = Camera.query()
        for camera in cameras:
            state = params_to_dict(camera.state)
            ## check if camera is online
            if state[ONLINE_PARAMETER_NAME] == 'true':
                if state.has_key(HARTBEAT_PARAMETER_NAME):
                    hartbeat_time = float(state[HARTBEAT_PARAMETER_NAME])
                    if ( (time.time() - hartbeat_time) > HARTBEAT_TIME_INTERVAL ):
                        self._set_camera_state(camera, ONLINE_PARAMETER_NAME, "false")
                        camera.put()
                        if state.has_key("email_alert_active") and state["email_alert_active"] == "true":
                            camera_name = ""
                            if state.has_key("device_title") and state["device_title"]:
                                camera_name = state["device_title"]
                            cam_auth_query = Credentials.query(Credentials.owner == camera.key)
                            cam_auth = cam_auth_query.get()
                            if cam_auth == None:
                                continue
                            mac_address = cam_auth.name

                            cuser = camera.owner.get()
                            receiver = cuser.name
                            phone_number = cuser.phone_number
                            telephone_co = cuser.telephone_co
                            timezone_str = ""
                            if state.has_key("camera_timezone_local_area") and state["camera_timezone_local_area"]:
                                timezone_str = state["camera_timezone_local_area"]
                            if state.has_key("camera_offline_alert_email") and state[
                                "camera_offline_alert_email"] == "true":
                                self._send_email_sms_alert("offline_alert_email", receiver, phone_number, telephone_co,
                                                           camera_name, mac_address, camera.key, timezone_str)
                            e("camera state: {0}".format(repr(state)))
                            if state.has_key("camera_offline_alert_sms") and state[
                                "camera_offline_alert_sms"] == "true":
                                e("camera_offline_alert_sms")
                                self._send_email_sms_alert("offline_alert_sms", receiver, phone_number, telephone_co,
                                                           camera_name, mac_address, camera.key, timezone_str)

        return ok()

    @cherrypy.expose
    def get_camera_state(self, camera_id=None, **kwargs):
        auth = self._authorize()
        if camera_id is not None:
            camkey = key_from_jsonparam(camera_id)
            camera = camkey.get()
        else:
            camera = auth.owner.get()
            if not isinstance(camera, Camera):
                return self.error("NOT APPLICABLE")
        ret = params_to_dict(camera.state)
        ## check if camera reported to server in given timeframe
        if ret.has_key(HARTBEAT_PARAMETER_NAME):
            last_hartbeat = float(ret[HARTBEAT_PARAMETER_NAME])
            if ((time.time() - last_hartbeat) > HARTBEAT_TIME_INTERVAL):
                ## camera did not reporrt it self on time, declaring it offline
                ret[ONLINE_PARAMETER_NAME] = "false"

        ## we do not want to set external ip of the client to the camera state,
        ## so we are counting that when the key is sent it is a camera requesting the state,
        ## if the id is sent then it is the client.
        if (camera_id == None):
            ## compare camera current external ip with actual external ip
            if (ret["external_ip"] != cherrypy.request.remote.ip):
                self._set_camera_state(camera, "external_ip", cherrypy.request.remote.ip)
                ret["external_ip"] = cherrypy.request.remote.ip
        return serve_result({"state": ret})

    @cherrypy.expose
    def reset_camera_state(self, camera_id=None, **kwargs):
        auth = self._authorize()
        if camera_id is not None:
            camkey = key_from_jsonparam(camera_id)
            camera = camkey.get()
        else:
            camera = auth.owner.get()
            if not isinstance(camera, Camera):
                return self.error("NOT APPLICABLE")
        self._default_camera_state(camera)
        camera.put()
        return ok()

    @cherrypy.expose
    def get_user_info(self, **kwargs):
        auth = self._authorize()
        user = auth.owner.get()
        if not isinstance(user, User):
            return self.error("NOT APPLICABLE")
        ret = {
            "name": user.name,
            "phone_number": user.phone_number,
            "telephone_co": user.telephone_co,
            "camera_count": user.camera_count,
            "media_count": user.media_count,
            "storage_used": user.storage_used
        }
        if (user.subscription_start_time != None):
            ret["subscription_start_time"] = user.subscription_start_time.strftime("%Y-%m-%d %H:%M:%S")
        if (user.subscription_end_time != None):
            ret["subscription_end_time"] = user.subscription_end_time.strftime("%Y-%m-%d %H:%M:%S")
        if (user.registered_on != None):
            ret["registered_on"] = user.registered_on.strftime("%Y-%m-%d %H:%M:%S")

        return serve_result({"state": ret})

    def _send_blob(self, blob_info, content_type):
        blob_key = blob_info.key()
        w("setting blobkey={0}".format(str(blob_key)))
        cherrypy.response.headers[blobstore.BLOB_KEY_HEADER] = str(blob_key)
        cherrypy.response.headers["Content-Type"] = content_type
        cherrypy.response.status = ""

    def _authorized_for_asset(self, blob_info, presented_key, model_attr):
        query = VideoSegment.query(model_attr == blob_info.key())
        segment = query.get()
        if not segment:
            e("/video(id={id}) segment missing".format(id=id))
            return False
        owner = segment.owner.get()
        if owner.key == presented_key:
            # this is camera itself
            w("authorized for asset - camera")
            return True
        if not isinstance(owner, Camera):
            e("/video(id={id}) segment owner is not Camera".format(id=id))
            return False
        if owner.owner == presented_key:
            # segment's camera owner
            w("authorized for asset - user")
            return True
        w("NOT authorized for asset")
        return False

    @cherrypy.expose
    def image(self, id, **kwargs):
        auth = self._authorize()
        try:
            id = base64.urlsafe_b64decode(str(id))
        except:
            return self.error('BROKEN LINK')
        thumbnail_blobinfo = blobstore.BlobInfo.get(id)
        if self._authorized_for_asset(thumbnail_blobinfo, auth.owner, VideoSegment.thumbnail_blobkey):
            self._send_blob(thumbnail_blobinfo, "image/jpeg")
        return ""

    @cherrypy.expose
    def video(self, id, **kwargs):
        auth = self._authorize()
        try:
            id = base64.urlsafe_b64decode(str(id))
        except:
            return self.error('BROKEN LINK')
        video_blobinfo = blobstore.BlobInfo.get(id)
        import info

        info.ViewLog.log(video_blobinfo)

        auth = self._authorize()
        if self._authorized_for_asset(video_blobinfo, auth.owner, VideoSegment.video_data_blobkey):
            # self._send_blob(video_blobinfo, "video/mp2t")
            self._send_blob(video_blobinfo, "video/x-flv")
        return ""

    @cherrypy.expose
    def username_available(self, email):
        email = str(email).lower()
        if not validate_email(email):
            return error("BAD EMAIL")
        query = Credentials.query(Credentials.name == email)
        entity = query.get()
        if entity:
            return error('DUPLICATE')
        return ok()


    def _get_camera_state_value(self, camera, name):
        for state in camera.state:
            if state.name == name:
                return state.value

    def _set_camera_state(self, camera, name, value):
        for state in camera.state:
            if state.name == name:
                state.value = str(value)
                return
        "Add parameter to camera only if it is a default value"
        if name in CAMERA_DEFAULT_VALUES.keys():
            camera.state.append(Parameter(name=name, value=str(value)))
        else:
            e(
                "Can not set parameter with name : " + name + " to camera state because it is not defined as camera default value !!!");

    def _send_gcm_message(self, camera, payload, collapse_key, expiry_in_seconds=300.0):
        w(" > > > push_token={0}".format(camera.gcm_push_token))
        gcm_message = GCMMessage(camera.gcm_push_token, payload, collapse_key=collapse_key)
        gcm_conn = GCMConnection()
        gcm_conn.notify_device(gcm_message)

        command = GCMCommand()
        command.camera = camera.key
        command.utc_expiry = datetime.utcnow() + timedelta(0, expiry_in_seconds)
        command.put()
        return command

    @cherrypy.expose
    def send_command(self, camera_id, payload, collapse_key=None, expiry_in_seconds=300.0, **kwargs):
        """
            We will use async requests for communicating with GCM if GCM provides synchronous calls, if not use tasks and queues like this http://stackoverflow.com/questions/4254678/app-engine-is-time-sleep-counting-towards-my-quotas
        """
        expiry_in_seconds = float(expiry_in_seconds)
        auth = self._authorize()
        camkey = key_from_jsonparam(camera_id)
        camera = camkey.get()
        if camera.owner != auth.owner:
            # you must own that camera
            return self.error("IDENTITY ERROR")

        e("send_command: {0}".format(repr(payload)))
        command = self._send_gcm_message(camera, payload, collapse_key, expiry_in_seconds)
        return serve_result({"command_id": key_to_jsonparam(command.key)})

    @cherrypy.expose
    def remove_camera_from_user(self, camera_id, admin_mode=False, **kwargs):
        """
            This is triggered by dashboard,
            meaning Admin is deleteing this camera for particualr user.
        """
        camkey = key_from_jsonparam(camera_id)
        camera = camkey.get()
        if not admin_mode:
            auth = self._authorize()
            if (camera.owner != auth.owner):
                # you must own that camera
                return self.error("IDENTITY ERROR")
        else:
            guser = users.get_current_user()
            if not guser or not users.is_current_user_admin():
                # you must be an admin
                return self.error("IDENTITY ERROR")
        return self._remove_camera_from_user(camera)

    @cherrypy.expose
    def remove_camera_from_this_user(self, camera_id, **kwargs):
        """
            This is triggered by one of the clients,
            meaning user is deleting one of his own cameras.
        """
        auth = self._authorize()
        user = auth.owner.get()
        if not isinstance(user, User):
            return self.error("NOT APPLICABLE")
        camkey = key_from_jsonparam(camera_id)
        camera = camkey.get()
        if (not camera.owner == user.key):
            return self.error("NOT APPLICABLE")
        if (camera == None):
            return self.error("NOT REGISTERED")
        return self._remove_camera_from_user(camera)

    def _remove_camera_from_user(self, camera):
        camera.deleted = True
        camera.put()
        cam_auth_query = Credentials.query(Credentials.owner == camera.key)
        cam_auth = cam_auth_query.get()
        if cam_auth:
            taskqueue.add(url='/dashboard/users/camera_increment?key=%s&rev=true' % (camera.owner.urlsafe()),
                          method='GET')
            cam_auth.key.delete()
            return ok()
        else:
            return error("MISSING LINK")

    def _default_camera_state(self, camera):
        del camera.state[:]
        for key in CAMERA_DEFAULT_VALUES.keys():
            camera.state.append(Parameter(name=key, value=CAMERA_DEFAULT_VALUES[key]))

    @cherrypy.expose
    def register_device(self, name, password, gcm_push_token=None, build_id="USA"):
        """
            IDH - independant design house
            so we need to map given range of SSID & MAC to allowed devices

            SSID & MAC are aquired from IDH
        """
        if gcm_push_token is None:
            gcm_push_token = password
        query = Credentials.query(Credentials.name == name)
        entity = query.get()
        if entity:
            e("DUPLICATE device registration name={n} pass={p}".format(n=name, p=password))
            return self.error('DUPLICATE')
        camera = Camera()
        camera.build_id = build_id
        camera.gcm_push_token = gcm_push_token
        self._default_camera_state(camera)
        camera.name = name
        camera.put()

        try:
            auth = Credentials()
            auth.name = name
            auth.password = password
            auth.challange = generate_challange()
            auth.owner = camera.key
            auth.put()
        except:
            camera.key.delete()
            raise

        return ok()


    @cherrypy.expose
    def user_subscription(self, key):
        auth = self._authorize()
        cam = auth.owner.get()
        user = cam.owner.get()

        subscription = "false"
        now = datetime.now()

        if user.subscription_end_time is not None:
            if user.subscription_end_time < now:
                subscription = "false"
            else:
                subscription = "true"
        else:
            subscription = "false"

        return serve_result({"subscription": subscription})

    def __pal(self):
        return PayPalInterface(config=PayPalConfig(
            API_USERNAME="chkindev-facilitator_api1.gmail.com",
            API_PASSWORD="BXVDCQENHTVE3KBQ",
            API_SIGNATURE="AFcWxV21C7fd0v3bYYYRCpSSRl31AKXfow2WSAN2MgZxTqnNv0-lx4wj",
            DEBUG_LEVEL=0,
        ))

    @cherrypy.expose
    def pay_canceled(self, **kwargs):
        i("you lousy bugger !")
        raise cherrypy.HTTPRedirect("/frontend/user.html")

    @cherrypy.expose
    def pay_confirmed(self, PayerID, token, **kwargs):
        e("pay_confirmed, do_express_checkout_payment() with paymentaction = 'Sale'. This will finalize and bill.")
        pal = self.__pal()
        details_response = pal.get_express_checkout_details(token=token)
        query = PaymentRecord.query(PaymentRecord.token == token)
        payment = query.get()
        user = payment.owner.get()
        payment.transaction_details_response = details_response.raw
        payment.put()
        ## {u'TAXAMT': [u'0.00'],
        ##  u'CORRELATIONID': [u'1d9431a917a66'],
        ##  u'SHIPTOSTATE': [u'CA'],
        ##  u'EMAIL': [u'testclient@chkincam.com'],
        ##  u'PAYMENTREQUEST_0_INSURANCEAMT': [u'0.00'],
        ##  u'PAYMENTREQUEST_0_SHIPTOZIP': [u'95131'],
        ##  u'CHECKOUTSTATUS': [u'PaymentActionNotInitiated'],
        ##  u'SHIPTOCITY': [u'San Jose'],
        ##  u'CURRENCYCODE': [u'USD'],
        ##  u'SHIPTOCOUNTRYCODE': [u'US'],
        ##  u'PAYMENTREQUEST_0_SHIPTOSTATE': [u'CA'],
        ##  u'VERSION': [u'98.0'],
        ##  u'PAYMENTREQUEST_0_CURRENCYCODE': [u'USD'],
        ##  u'PAYMENTREQUEST_0_SHIPPINGAMT': [u'0.00'],
        ##  u'BILLINGAGREEMENTACCEPTEDSTATUS': [u'0'],
        ##  u'PAYMENTREQUEST_0_SHIPTONAME': [u'alex alex'],
        ##  u'PAYMENTREQUEST_0_AMT': [u'10.00'],
        ##  u'PAYMENTREQUEST_0_ADDRESSNORMALIZATIONSTATUS': [u'None'],
        ##  u'SHIPDISCAMT': [u'0.00'],
        ##  u'PAYMENTREQUEST_0_SHIPTOCOUNTRYNAME': [u'United States'],
        ##  u'TIMESTAMP': [u'2014-07-17T14:21:17Z'],
        ##  u'PAYERSTATUS': [u'verified'],
        ##  u'LASTNAME': [u'alex'],
        ##  u'SHIPTONAME': [u'alex alex'],
        ##  u'ADDRESSSTATUS': [u'Confirmed'],
        ##  u'COUNTRYCODE': [u'US'],
        ##  u'AMT': [u'10.00'],
        ##  u'PAYMENTREQUEST_0_TAXAMT': [u'0.00'],
        ##  u'SHIPTOCOUNTRYNAME': [u'United States'],
        ##  u'INSURANCEAMT': [u'0.00'],
        ##  u'TOKEN': [u'EC-5EM85976686342813'],
        ##  u'PAYMENTREQUESTINFO_0_ERRORCODE': [u'0'],
        ##  u'PAYMENTREQUEST_0_SHIPTOCOUNTRYCODE': [u'US'],
        ##  u'PAYMENTREQUEST_0_INSURANCEOPTIONOFFERED': [u'false'],
        ##  u'SHIPPINGAMT': [u'0.00'],
        ##  u'BUILD': [u'11843215'],
        ##  u'ACK': [u'Success'],
        ##  u'PAYERID': [u'ABMTAF6HN3RVG'],
        ##  u'SHIPTOZIP': [u'95131'],
        ##  u'PAYMENTREQUEST_0_SHIPTOSTREET': [u'1 Main St'],
        ##  u'PAYMENTREQUEST_0_HANDLINGAMT': [u'0.00'],
        ##  u'PAYMENTREQUEST_0_ADDRESSSTATUS': [u'Confirmed'],
        ##  u'FIRSTNAME': [u'alex'],
        ##  u'PAYMENTREQUEST_0_SHIPTOCITY': [u'San Jose'],
        ##  u'HANDLINGAMT': [u'0.00'],
        ##  u'PAYMENTREQUEST_0_SHIPDISCAMT': [u'0.00'],
        ##  u'SHIPTOSTREET': [u'1 Main St']}
        response = pal.do_express_checkout_payment(
            token=token,
            paymentaction="Sale",
            payerid=details_response.payerid,
            amt=details_response.amt,
        )
        payment.execution_response = response.raw
        payment.put()
        ### https://developer.paypal.com/docs/classic/api/merchant/DoExpressCheckoutPayment_API_Operation_NVP/
        ## {u'PAYMENTINFO_0_FEEAMT': [u'0.59'],
        ##  u'PAYMENTINFO_0_TRANSACTIONTYPE': [u'expresscheckout'],
        ##  u'TOKEN': [u'EC-55039542R0257530S'],
        ##  u'PAYMENTINFO_0_ORDERTIME': [u'2014-07-17T14:24:55Z'],
        ##  u'REASONCODE': [u'None'],
        ##  u'PAYMENTINFO_0_PAYMENTTYPE': [u'instant'],
        ##  u'PAYMENTINFO_0_CURRENCYCODE': [u'USD'],
        ##  u'PAYMENTINFO_0_PROTECTIONELIGIBILITY': [u'Eligible'],
        ##  u'TRANSACTIONTYPE': [u'expresscheckout'],
        ##  u'PAYMENTINFO_0_PROTECTIONELIGIBILITYTYPE': [u'ItemNotReceivedEligible,UnauthorizedPaymentEligible'],
        ##  u'TRANSACTIONID': [u'04W209885E478981K'],
        ##  u'PROTECTIONELIGIBILITY': [u'Eligible'],
        ##  u'SUCCESSPAGEREDIRECTREQUESTED': [u'false'],
        ##  u'PAYMENTINFO_0_ERRORCODE': [u'0'],
        ##  u'CORRELATIONID': [u'53b075967cc0'],
        ##  u'TIMESTAMP': [u'2014-07-17T14:24:55Z'],
        ##  u'PAYMENTINFO_0_ACK': [u'Success'],
        ##  u'TAXAMT': [u'0.00'],
        ##  u'PAYMENTINFO_0_PENDINGREASON': [u'None'],
        ##  u'INSURANCEOPTIONSELECTED': [u'false'],
        ##  u'PAYMENTTYPE': [u'instant'],
        ##  u'PAYMENTSTATUS': [u'Completed'],
        ##  u'PAYMENTINFO_0_PAYMENTSTATUS': [u'Completed'],
        ##  u'PAYMENTINFO_0_TAXAMT': [u'0.00'],
        ##  u'FEEAMT': [u'0.59'],
        ##  u'ACK': [u'Success'],
        ##  u'CURRENCYCODE': [u'USD'],
        ##  u'PAYMENTINFO_0_REASONCODE': [u'None'],
        ##  u'VERSION': [u'98.0'],
        ##  u'ORDERTIME': [u'2014-07-17T14:24:55Z'],
        ##  u'AMT': [u'10.00'],
        ##  u'PAYMENTINFO_0_AMT': [u'10.00'],
        ##  u'PENDINGREASON': [u'None'],
        ##  u'PAYMENTINFO_0_TRANSACTIONID': [u'04W209885E478981K'],
        ##  u'BUILD': [u'11843215'],
        ##  u'PAYMENTINFO_0_SECUREMERCHANTACCOUNTID': [u'RL3LLB75U5TDC'],
        ##  u'SHIPPINGOPTIONISDEFAULT': [u'false']}
        if response["PAYMENTINFO_0_ACK"] == u'Success' and response["PAYMENTINFO_0_PAYMENTSTATUS"] == u'Completed':
            # increase subscription date
            now = datetime.now()
            w("[PAYMENT] subscription before payment start={0} end={1} token={2}".format(user.subscription_start_time,
                                                                                         user.subscription_end_time,
                                                                                         token))

            if user.subscription_end_time is None or user.subscription_start_time is None or user.subscription_end_time < now:
                user.subscription_start_time = now
                user.subscription_end_time = now + timedelta(days=payment.plan_days)
                user.put()
            else:  #elif user.subscription_end_time > now:
                # just prolong it
                user.subscription_end_time += timedelta(days=payment.plan_days)
                user.put()
            w("[PAYMENT] subscription after payment start={0} end={1} token={2}".format(user.subscription_start_time,
                                                                                        user.subscription_end_time,
                                                                                        token))
            # Success
            raise cherrypy.HTTPRedirect("/frontend/user.html")
        # something went wrong
        e("[PAYMENT] ERROR: something went wrong check this manually")
        raise cherrypy.HTTPRedirect("/frontend/user.html")

    EXAMPLE_USER_EMAIL = "testclient@chkincam.com"

    @cherrypy.expose
    def pay(self, plan):

        now = datetime.now()
        price = 0
        days = 0
        description = ""
        start = now
        if plan == "month":
            days = 30
            price = 10.00
            description = "Monthly plan $10.00"
        elif plan == "year":
            days = 365
            price = 100.00
            description = "Yearly plan $100.00"

        end = start + timedelta(days=days)
        #end = start + timedelta(days=1)

        e("[PAYMENT] request pay {0} plan price={1}".format(plan, price))

        auth = self._authorize()
        user = auth.owner.get()
        if not isinstance(user, User):
            e("[PAYMENT] not authorized as user, name={0} plan={1} end={2}".format(auth.name, plan))
            # TODO: Render HTML instead of json response
            return self.error("NOT APPLICABLE")

        # contact paypal and redirect user to payment form
        pal = self.__pal()
        response = pal.set_express_checkout(
            PAYMENTREQUEST_0_AMT=str(price),
            PAYMENTREQUEST_0_PAYMENTACTION='Order',
            PAYMENTREQUEST_0_DESC=description,  #amt=str(price),  #paymentaction='Order',  #desc=description,
            returnurl="http://{host}:{port}/pay_confirmed".format(host=SERVER_HOSTNAME, port=SERVER_PORT),
            cancelurl="http://{host}:{port}/pay_canceled".format(host=SERVER_HOSTNAME, port=SERVER_PORT),
            email=auth.name
        )
        assert response is not None, "response is None"
        payment = PaymentRecord()
        payment.token = response.token
        payment.start = start
        payment.end = end
        payment.price = price
        payment.plan_type = plan
        payment.plan_days = days
        payment.checkout_setup_response = response.raw
        payment.owner = user.key
        payment.put()

        ## inspect response.raw dict:
        ##		{u'VERSION': [u'98.0'],
        ##		 u'TOKEN': [u'EC-2H501994NU741124E'],
        ##		 u'BUILD': [u'11843215'],
        ##		 u'CORRELATIONID': [u'a25d1539f189a'],
        ##		 u'TIMESTAMP': [u'2014-07-17T14:13:53Z'],
        ##		 u'ACK': [u'Success']}
        # Redirect your client to this URL for approval.
        redirect_url = pal.generate_express_checkout_redirect_url(response.token)

        raise cherrypy.HTTPRedirect(redirect_url)

    @cherrypy.expose
    def subscription_form(self):
        return """
<html>
<head>
	<title>Pay subscription</title>
</head>
<body>
	Renew your subscription:
	<form action="/pay">
		<input type="submit" value="PAY" />
	</form>
</body>
</html>
		"""

    @cherrypy.expose
    def delete_user_task(self, user_key, **kwargs):
        user = ndb.Key(urlsafe=user_key).get()
        for cam in Camera.query(Camera.owner == user.key):
            for media in VideoSegment.query(VideoSegment.owner == cam.key):
                if media.video_data_blobkey != None:
                    blobstore.delete(media.video_data_blobkey)
                if media.thumbnail_blobkey != None:
                    blobstore.delete(media.thumbnail_blobkey)
                media.deleted = True
                media.put()
        return ok()

    ##
    ##
    ## Debugging:
    ##
    ##
    @cherrypy.expose
    def delete_user(self, email, admin_mode=False, **kwargs):
        email = str(email).lower()
        query = Credentials.query(Credentials.name == email)
        user_auth = query.get()
        if not user_auth:
            return self.error("NOT REGISTERED")
        user = user_auth.owner.get()
        if not isinstance(user, User):
            return self.error("NOT APPLICABLE")

        if not admin_mode:
            auth = self._authorize()
            if (user_auth.owner != auth.owner):
                # you must be that user
                return self.error("IDENTITY ERROR")
        else:
            guser = users.get_current_user()
            if not guser or not users.is_current_user_admin():
                # you must be an admin
                return self.error("IDENTITY ERROR")

        taskqueue.add(url='/delete_user_task', params={'user_key': (user.key.urlsafe())})

        # "deleting" the user's cameras
        for cam in Camera.query(Camera.owner == user.key):
            cam.deleted = True
            cam.put()
            cam_auth_query = Credentials.query(Credentials.owner == cam.key)
            cam_auth = cam_auth_query.get()
            if cam_auth:
                cam_auth.key.delete()

        # "deleting" user
        user_auth.key.delete()
        user.camera_count = 0
        user.media_count = 0
        user.storage_used = 0
        user.deleted = True
        user.put()
        return ok()

    @cherrypy.expose
    def request_update_upload_url(self, **kwargs):
        auth = self._authorize()
        user = auth.owner.get()
        ## TODO if user name is not in admin list then reject request
        return serve_result({
            "upload_url": blobstore.create_upload_url('/push_apk_blobed'),
        })

    @cherrypy.expose
    def what_is_the_latest_apk_version(self, build_id, **kwargs):
        auth = self._authorize()
        query = ChkinAPK.query(ChkinAPK.build_id == build_id)
        apk = query.order(-ChkinAPK.uploaded).get()
        if (apk == None):
            return ErrorConvention("NOT APPLICABLE")
        else:
            return serve_result({
                "latest_version": apk.version,
            })

    @cherrypy.expose
    def download_latest_apk(self, build_id, **kwargs):
        auth = self._authorize()
        query = ChkinAPK.query(ChkinAPK.build_id == build_id)
        apk = query.order(-ChkinAPK.uploaded).get()
        apk_blob_info = blobstore.BlobInfo.get(apk.apk_blobkey)
        self._send_blob(apk_blob_info, "application/vnd.android.package-archive")

    # for debug only
    @cherrypy.expose
    def register_user(self, name, password):
        ## all names should be case insenssitive
        name = str(name).lower()
        # check if name i valid email format
        if not validate_email(name):
            return error("BAD EMAIL")
        # TODO: send confirmation email

        ## check for duplicate name
        # query = Credentials.all(keys_only=True).filter('name', name)
        query = Credentials.query(Credentials.name == name)
        entity = query.get()
        if entity:
            return ErrorConvention('DUPLICATE')

        user = User()
        user.name = name

        # for testing in chkincamdev.com server
        # ## set developer subscription by default for testing pourposes
        # packet = Packet.query(Packet.name == "Developer").get()
        # if packet is None:
        # 	packet = Packet()
        # 	packet.name = "Developer"
        # 	packet.put()
        # subscriptions = Subscription.query().fetch()
        # subscription = None
        # if len(subscriptions) ==  0:
        # 	subscription = Subscription()
        # 	subscription.packets.append(packet.key)
        # 	subscription.put()
        # else:
        # 	for s in subscriptions:
        # 		for p in s.packets:
        # 			if p.get().name == "Developer":
        # 				subscription = s

        # user.subscription = subscription.key
        # user.subscription_start_time = datetime.utcnow()
        # subscription_duration_in_seconds = 60 * 60 * 24 * 365 ## one year
        # user.subscription_end_time = datetime.utcnow() + timedelta(0, subscription_duration_in_seconds)


        user.registered_on = datetime.utcnow()
        user.put()

        try:
            auth = Credentials()
            auth.name = name
            auth.password = password
            auth.challange = generate_challange()
            auth.owner = user.key
            auth.put()
        except:
            user.key.delete()
            raise

        return ok()

    @cherrypy.expose
    def key(self):
        # return str(dir(cherrypy.request.cookie["key"]))
        if cherrypy.request.cookie.has_key("key"):
            return str(cherrypy.request.cookie["key"].value)
        else:
            return "/"

    @cherrypy.expose
    def camera_heartbeat(self, key):
        auth = self._authorize()
        cam = auth.owner.get()
        self._set_camera_state(cam, ONLINE_PARAMETER_NAME, "true")
        current_time = str(time.time())
        self._set_camera_state(cam, HARTBEAT_PARAMETER_NAME, current_time)
        cam.put()
        return ok()

    @cherrypy.expose
    def is_camera_claimed(self, key, name):
        auth = self._authorize()
        cam = auth.owner.get()
        camOwner = cam.owner
        if (camOwner != None):
            userCred = Credentials.query(Credentials.name == name).get()
            if (userCred != None):
                userOwner = userCred.owner
                if (userOwner != None):
                    if camOwner.id() == userOwner.id():
                        return ok()
        return error("NOT APPLICABLE")


    @cherrypy.expose
    def get_username_for_claimed_camera(self, key):
        auth = self._authorize()
        cam = auth.owner.get()
        camOwner = cam.owner
        userCred = Credentials.query(Credentials.owner == camOwner).get()
        if (userCred == None):
            return error("NOT APPLICABLE")
        else:
            return serve_result({
                "camera_owner": userCred.name,
            })

    @cherrypy.expose
    def change_user_password(self, key, password):
        auth = self._authorize()
        user = auth.owner.get()
        if user is None:
            return error("NOT REGISTERED")
        if not isinstance(user, User):
            return error("NOT APPLICABLE")
        ## change password
        auth.password = password
        auth.put()
        ## Notify al devices that password have changed
        cameras = Camera.query(Camera.owner == auth.owner)
        payload = dumps({
            "action": GCM_ACTION_KEY_PASSWORD_CHANGED,
            GCM_PASSWORD_KEY: password
        })
        for camera in cameras:
            self._send_gcm_message(camera, payload, GCM_PASSWORD_CHANGED_COLLAPSE_KEY, -1)
        return ok()


    @cherrypy.expose
    def send_gcm_to_client(self, push_token, payload, collapse_key=None, expiry_in_seconds=300.0, **kwargs):
        expiry_in_seconds = float(expiry_in_seconds)
        auth = self._authorize()
        w(" > > > push_token={0}".format(push_token))
        gcm_message = GCMMessage(push_token, payload, collapse_key=collapse_key)
        gcm_conn = GCMConnection()
        gcm_conn.notify_device(gcm_message)
        return ok()


    @cherrypy.expose
    def remove_video_from_user(self, video_id, admin_mode=False, **kwargs):
        video_key = key_from_jsonparam(video_id)
        video = video_key.get()
        if not admin_mode:
            auth = self._authorize()
            if (video.user != auth.owner):
                # you must own that camera
                return self.error("IDENTITY ERROR")
        else:
            guser = users.get_current_user()
            if not guser or not users.is_current_user_admin():
                # you must be an admin
                return self.error("IDENTITY ERROR")

        if video.video_data_blobkey != None:
            info = blobstore.BlobInfo.get(video.video_data_blobkey)
            taskqueue.add(
                url='/dashboard/users/media_increment?key=%s&size=%s&rev=true' % (video.user.urlsafe(), info.size),
                method='GET')
            blobstore.delete(video.video_data_blobkey)
        if video.thumbnail_blobkey != None:
            blobstore.delete(video.thumbnail_blobkey)
        video.deleted = True
        video.put()
        # 		print video
        return ok()


    @cherrypy.expose
    def client_logged_in(self, key, **kwargs):
        auth = self._authorize()
        user = auth.owner.get()
        if user is None:
            return error("NOT REGISTERED")
        if not isinstance(user, User):
            return error("NOT APPLICABLE")

        session = ClientSession()
        session.owner = user.key
        session.logged_in = datetime.fromtimestamp(float(time.time()))
        session.ip_address = cherrypy.request.remote.ip
        # assuming that incoming key and session_key are same.
        session.session_key = auth.session_key
        session.expires_at = auth.session_created + timedelta(0, SESSION_KEY_ROTATION_TIME_INTERVAL)
        session.client = cherrypy.request.headers.get('User-Agent', 'Unknown')
        session.put()
        return ok()


    @cherrypy.expose
    def client_logged_out(self, key, **kwargs):
        auth = self._authorize()
        user = auth.owner.get()
        if user is None:
            return error("NOT REGISTERED")
        if not isinstance(user, User):
            return error("NOT APPLICABLE")
        client = cherrypy.request.headers.get('User-Agent', 'Unknown')
        session = ClientSession.query(ClientSession.client == client,
                                      ClientSession.owner == user.key,
                                      ClientSession.logged_out == None).order(-ClientSession.logged_in).get()
        if (session is None):
            return error("SESSION NOT FOUND")

        session.logged_out = datetime.fromtimestamp(float(time.time()))
        session.put()
        return ok()


    @cherrypy.expose
    def number_of_connected_clients(self, key, **kwargs):
        auth = self._authorize()
        user = auth.owner.get()
        if user is None:
            return error("NOT REGISTERED")
        if not isinstance(user, User):
            return error("NOT APPLICABLE")
        sessions = ClientSession.query(ClientSession.logged_out == None,
                                       ClientSession.owner == user.key).order(-ClientSession.logged_in)

        result = []
        for session in sessions:
            if session.expires_at >= datetime.now():
                session_as_dict = {}
                session_as_dict["ip_address"] = session.ip_address
                session_as_dict["logged_in"] = calendar.timegm(session.logged_in.timetuple())
                session_as_dict["client"] = session.client
                result.append(session_as_dict)
            else:
                # we need to do some more improvements on log out management
                session.logged_out = datetime.now()
                session.put()


        # check for last session ip
        last_session = ClientSession.query(ClientSession.logged_out != None, ClientSession.owner == user.key).order(
            -ClientSession.logged_out).get()
        last_session_ip = last_session.ip_address if last_session else None
        return serve_result({"count": len(result) or 1, "sessions": result, "last_session_ip": last_session_ip})


    @cherrypy.expose
    def forgot_password(self, email, captcha_response):
        """
        Sends Username & Password to user
        args:
        - email
        - captcha response
        """
        e(captcha_response)
        g_response = get("""\
https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}""".format(CAPTCHA_SECRET,
                                                                                  captcha_response)).json()
        # json response would be in the format of {u'success': True}

        if (g_response['success']):
            e(email)
            credential = Credentials.query(Credentials.name == email).get()
            if credential:
                subject = "Request for forgotten password"
                body = """\
You ({email}) have asked us for your password. You can find the credential for your account below:
Username: {email},
Password: {password}

Please note that Username and Password are case sensitive.
""".format(email=credential.name, password=credential.password)
                mail.send_mail(SERVER_SENDER_ADDRESS, credential.name, subject, body)
                w(body)
                return ok()
            else:
                return error("NOT REGISTERED")
        else:
            return error("CAPTCHA VERIFICATION FAILED")


    @cherrypy.expose
    def forgot_password_form(self, **kwargs):
        raise cherrypy.HTTPRedirect("/frontend/forgot_pass.html")


    @cherrypy.expose
    def session_expired(self):
        auth = self._authorize()
        return ok()


app_conf = {
    '/': {
        # 'tools.sessions.on': True,
        'tools.staticdir.root': os.path.abspath(os.getcwd()),
    },
    '/frontend': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': './frontend'
        # 'tools.staticdir.dir': os.path.join(os.path.abspath(os.getcwd()), 'frontend'),
    }
}


if __name__ == "__main__":
    cherrypy.quickstart(App(), '/', app_conf)
    # app = cherrypy.tree.mount(App(), '/')
# api = App()
# app = cherrypy.tree.mount(AppWrapper(api), '/')
# run_wsgi_app(app)

# if __name__ == "__main__":
#     app = webapp2.WSGIApplication([
#         ('/q/chl', Authenticate),
#         ('/chl', Authenticate),
#     ], debug=True)
