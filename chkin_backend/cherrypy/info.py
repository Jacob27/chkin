# from google.appengine.ext import ndb
# from google.appengine.api import memcache
# from google.appengine.datastore.datastore_query import Cursor

# import ndbpager
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
import main

from datetime import datetime
from datetime import timedelta

storage_cost = 0.026
downloaded_cost = 0.12
day = timedelta(1)
month = timedelta(30)

Base = declarative_base()


def calculate_cost(storage, downloaded):
    return storage * storage_cost / 1073741824.0 + downloaded * downloaded_cost / 1073741824.0


class UserCount(Base):
    __tablename__ = 'user_count'

    id = Column(Integer, primary_key=True)
    date = Column(Date)
    # @TODO find a way to replace in Alchemy
    # day = ndb.ComputedProperty(lambda self: self.date.day)
    # month = ndb.ComputedProperty(lambda self: self.date.month)
    # day_of_week = ndb.ComputedProperty(lambda self: self.date.weekday())
    count = Column(Integer)

    @classmethod
    def log(cls, count, date=None):
        if date == None:
            date = datetime.now().date()
        ent = cls(date=date, count=count)
        ent.put()

    @classmethod
    def summary(cls):
        ents = [ent for ent in list(cls.query(projection=["date", "count"]).order(cls.date))]
        return ents

    @classmethod
    def clear(cls):
        ndb.delete_multi(
            cls.query().fetch(keys_only=True)
        )


class UsageLog(Base):
    __tablename__ = 'user_log'

    id = Column(Integer, primary_key=True)
    date = Column(DateTime)
    # @TODO find a way to replace in Alchemy
    # day = ndb.ComputedProperty(lambda self: self.date.day)
    # month = ndb.ComputedProperty(lambda self: self.date.month)
    # day_of_week = ndb.ComputedProperty(lambda self: self.date.weekday())
    type = Column(String)
    # @TODO find a way to replace in Alchemy
    # key = ndb.KeyProperty()
    name = Column(String)
    storage = Column(Integer)
    downloaded = Column(Integer)

    @classmethod
    def summary(cls):
        ents = [ent for ent in list(cls.query().filter(cls.type == 'daily').order(cls.date))]
        return ents

    @classmethod
    def all(cls):
        ents = [ent for ent in list(cls.query())]
        return ents

    @classmethod
    def history(cls, key):
        rev_key = ndb.Key(urlsafe=key)
        now = datetime.now()
        base = datetime(now.year, now.month, now.day)

        ents = [ent for ent in list(
            cls.query().filter(cls.type == 'daily', cls.key == rev_key, cls.date >= base - month).order(cls.date))]
        return ents


class ViewLog(Base):
    __tablename__ = 'view_log'

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    # @TODO find a way to replace in Alchemy
    # user_key = ndb.KeyProperty()
    username = Column(String)
    # @TODO find a way to replace in Alchemy
    # camera_key = ndb.KeyProperty()
    camera = Column(String)
    # @TODO find a way to replace in Alchemy
    # video = ndb.KeyProperty()
    size = Column(Integer)

    @classmethod
    def log(cls, videoblob, timestamp=None):
        if timestamp == None:
            timestamp = datetime.now()

        resp = main.VideoSegment.query(main.VideoSegment.video_data_blobkey == videoblob.key()).fetch(1)
        if len(resp) > 0:
            video = resp[0]
            print 'VIDEO', video
            ent = cls(timestamp=timestamp, user_key=video.user, username=video.username, camera_key=video.owner,
                      camera=video.camera, video=video.key, size=video.size)
            ent.put()


class BaseInfo(object):
    pass


default_cost = {'daily': 0.0, 'weekly': 0.0, 'monthly': 0.0}

default_usage = {
'storage': {'daily': 0, 'weekly': 0, 'monthly': 0},
'downloaded': {'daily': 0, 'weekly': 0, 'monthly': 0},
}


class UserInfo(BaseInfo):
    @classmethod
    def summary(cls):
        resp = []
        for ent in main.User.query(main.User.deleted == None).iter(batch_size=100):
            if ent.usage != None:
                cost = {
                'daily': calculate_cost(ent.usage.storage.daily, ent.usage.downloaded.daily),
                'weekly': calculate_cost(ent.usage.storage.weekly, ent.usage.downloaded.weekly),
                'monthly': calculate_cost(ent.usage.storage.monthly, ent.usage.downloaded.monthly),
                }
                usage = ent.usage.json()
            else:
                cost = default_cost
                usage = default_usage

            resp.append({'key': ent.key.urlsafe(), 'id': ent.name, 'camera_count': ent.camera_count,
                         'media_count': ent.media_count, 'storage_used': ent.storage_used,
                         'deleted': ent.deleted == True, 'usage': usage, 'cost': cost})
        return resp


class CameraInfo(BaseInfo):
    @classmethod
    def summary(cls):
        resp = []
        for ent in main.Camera.query(main.Camera.deleted == None).iter(batch_size=100):
            if ent.usage != None:
                cost = {
                'daily': calculate_cost(ent.usage.storage.daily, ent.usage.downloaded.daily),
                'weekly': calculate_cost(ent.usage.storage.weekly, ent.usage.downloaded.weekly),
                'monthly': calculate_cost(ent.usage.storage.monthly, ent.usage.downloaded.monthly),
                }
                usage = ent.usage.json()
            else:
                cost = default_cost
                usage = default_usage

            has_cvr_subscription = False
            subscription_start_time = None
            subscription_end_time = None
            registered_since = None

            if ent:
                registered_since = ent.creation_date.strftime("%Y-%m-%d") if ent.creation_date else None

            if ent.owner:
                owner = ent.owner.get()
                subscription_start_time = owner.subscription_start_time.strftime(
                    "%Y-%m-%d") if owner.subscription_start_time else None
                subscription_end_time = owner.subscription_end_time if owner.subscription_end_time else None

                if subscription_end_time:
                    has_cvr_subscription = subscription_end_time > datetime.now()
                    subscription_end_time = subscription_end_time.strftime("%Y-%m-%d")

            parameters = {}
            if ent.state:
                for p in ent.state:
                    parameters[p.name] = p.value

            resp.append({'key': ent.key.urlsafe(), 'id': ent.name, 'owner': ent.user,
                         'parameters': parameters, 'deleted': ent.deleted == True,
                         'usage': usage, 'cost': cost,
                         'subscription_start_time': subscription_start_time,
                         'has_cvr_subscription': has_cvr_subscription,
                         'subscription_end_time': subscription_end_time,
                         'registered_since': registered_since,
                         'multicamera_discount': 'NA', 'coupon_code': 'NA'
            })

        return resp


class MediaInfo(BaseInfo):
    @classmethod
    def set_size(cls, key, size):
        rev_key = ndb.Key(urlsafe=key)
        obj = rev_key.get()
        obj.size = int(size)
        obj.put()

    @classmethod
    def blob_list(cls):
        resp = []
        count = 0
        for ent in main.VideoSegment.query(main.VideoSegment.video_data_blobkey != None).iter(batch_size=100):
            # count += 1
            # 			if count > 10:
            # 				break
            resp.append({'key': ent.key.urlsafe(), 'blobkey': ent.video_data_blobkey})
        return resp

    @classmethod
    def summary(cls, key=None):
        resp = []
        count = 0
        if key == None:
            q = main.VideoSegment.query()
        else:
            rev_key = ndb.Key(urlsafe=key)
            q = main.VideoSegment.query(main.VideoSegment.user == rev_key, main.VideoSegment.deleted == None)

        for ent in q.order(-main.VideoSegment.size).iter(batch_size=100):
            count += 1
            if count >= 100:
                break
            resp.append({'title': ent.video_name, 'user': ent.username, 'camera': ent.camera, 'duration': ent.duration,
                         'record_date': str(ent.utc_start), 'size': ent.size, 'deleted': ent.deleted == True})
        return resp

    sort_order = {
    'title': {'asc': main.VideoSegment.video_name, 'desc': -main.VideoSegment.video_name},
    'user': {'asc': main.VideoSegment.username, 'desc': -main.VideoSegment.username},
    'camera': {'asc': main.VideoSegment.camera, 'desc': -main.VideoSegment.camera},
    'record_date': {'asc': main.VideoSegment.utc_start, 'desc': -main.VideoSegment.utc_start},
    'duration': {'asc': main.VideoSegment.duration, 'desc': -main.VideoSegment.duration},
    'size': {'asc': main.VideoSegment.size, 'desc': -main.VideoSegment.size},
    }

    @classmethod
    def ajax(cls, sort, dir, page_size, page=1, key=None):
        if page_size == -1:
            page_size = 50
            # print 'AJAX', sort, dir, page_size, page, cls.sort_order[sort][dir]
        resp = []
        count = 0
        if key == None:
            query = main.VideoSegment.query(main.VideoSegment.deleted == None)
        else:
            rev_key = ndb.Key(urlsafe=key)
            query = main.VideoSegment.query(main.VideoSegment.user == rev_key, main.VideoSegment.deleted == None)
        query = query.order(cls.sort_order[sort][dir], main.VideoSegment.key)

        pager = ndbpager.Pager(multiplex='%s:%s:%s' % (key, sort, dir), query=query, page=page)
        entities, _, _ = pager.paginate(page_size=page_size)

        for ent in entities:
            delet_btn = '<input type="checkbox" class="iCheck select-action" data-id="%s">' % ent.key.urlsafe()
            video_name = ent.video_name if ent.video_name != None and ent.video_name != '' else 'VideoSegment_%s.flv' % ent.key.id()
            if ent.deleted == True:
                video_name = '<del>%s</del>' % video_name

            video_name = '<a href="/dashboard/media/serve/%s/%s">%s</a>' % (
            ent.video_data_blobkey, video_name, video_name)

            resp.append([delet_btn, video_name, ent.username, ent.camera, str(ent.utc_start), ent.duration,
                         '{0:,}'.format(int(ent.size / 1024)) if ent.size != None else None])
        return resp
