/**
 * CameraView is using knockout.js
 * That means that html tags are bound to this objects
 */

/**
 * Help function to validate true nd "true"
 * @param val
 * @returns {boolean}
 */
function toBool(val) {
	return val == "true";
}

function toString(val) {
	if (val == undefined) {
		return "";
	} else
		return String(val);
}

function toUpperCase(str) {
	if (str == undefined) {
		return "";
	} else {
		return str.toUpperCase();
	}
}

/**
 * Fake data for camera init
 * @type {{id: string, state: {external_ip: string, external_port: string, internal_ip: string, internal_port: string, microphone: string, motion_sensor: string, night_vision: boolean, recording_quality: string, speaker: string}}}
 */
var empty_data = {
	id: "test",
	state: {
		external_ip: "",
		external_port: "",
		internal_ip: "",
		internal_port: "",
		microphone: "",
		motion_sensor: "",
		online: "",
		recording_mode: "",
		night_vision: "",
		recording_quality: "",
		speaker: "",
		device_title: "",
		schedule_from: "02:33 AM",
		schedule_to: "15:41 PM",
		schedule_active: [],
		wakeup_auto_monitor: "",
	}
};

/**
 * Data's for one camera
 * - change on this object is reflected directly on html tags trough data-bind attribute (knockout.js)
 * @param data
 * @constructor
 */
function CameraData(data) {
	var self = this;
	// Check if data are valid
	var schedule_by_days = [];
	if(data.state.schedule_by_days !== undefined && data.state.schedule_by_days !== ""){
		schedule_by_days = data.state.schedule_by_days.split(",");
	}
	//data.user_email = data.user_email === undefined || data.user_email === null ? "Not available" : data.user_email;
	//data.phone_number = data.phone_number === undefined || data.phone_number === null ? "" : data.phone_number;

	/**
	 * All segments that are available for this camera
	 */
	self.segments = ko.observableArray([
		{'thumbnail_url': ''}
	]);
	/**
	 * Id of camera
	 */
	self.id = ko.observable(data.id);
	self._name = ko.observable("-");
	//self.user_email = ko.observable(data.user_email);
	//self.phone_number = ko.observable(data.phone_number);

	/**
	 * State of general settings for this camera
	 * @type {{external_ip: *, external_port: *, internal_ip: *, internal_port: *, microphone: *, motion_sensor: *, night_vision: *, recording_quality: *, speaker: *}}
	 */
	self.state = {
		"chkin_version": ko.observable(data.state.chkin_version),
		"external_ip": ko.observable(data.state.external_ip),
		"external_port": ko.observable(data.state.external_port),
		"internal_ip": ko.observable(data.state.internal_ip),
		"internal_port": ko.observable(data.state.internal_port),
		"microphone": ko.observable(toBool(data.state.microphone)),
		"motion_sensor": ko.observable(toBool(data.state.motion_sensor)),
		"night_vision": ko.observable(toBool(data.state.night_vision)),
		"online": ko.observable(toBool(data.state.online)),
		"recording_mode": ko.observable(toBool(data.state.recording_mode)),
		"recording_quality": ko.observable(data.state.recording_quality === "HD"),
		"speaker": ko.observable(toBool(data.state.speaker)),
		"camera_timezone_time_diff": ko.observable(data.state.camera_timezone_time_diff  || 0),
		"camera_timezone_local_area": ko.observable(data.state.camera_timezone_local_area),
		"schedule_active": ko.observable(toBool(data.state.schedule_active)),
		"schedule_from": ko.observable(data.state.schedule_from),
		"schedule_to": ko.observable(data.state.schedule_to),
		"schedule_by_days": ko.observableArray(schedule_by_days),
		"email_alert_active": ko.observable(toBool(data.state.email_alert_active)),
		"device_title": ko.observable(data.state.device_title),
		"device_address": ko.observable(data.state.device_address),

		"motion_event_alert_email": ko.observable(toBool(data.state.motion_event_alert_email)),
		"loud_noise_alert_email": ko.observable(toBool(data.state.loud_noise_alert_email)),
		"camera_offline_alert_email": ko.observable(toBool(data.state.camera_offline_alert_email)),

		"motion_event_alert_sms": ko.observable(toBool(data.state.motion_event_alert_sms)),
		"loud_noise_alert_sms": ko.observable(toBool(data.state.loud_noise_alert_sms)),
		"camera_offline_alert_sms": ko.observable(toBool(data.state.camera_offline_alert_sms)),
		"wakeup_auto_monitor": ko.observable(toBool(data.state.wakeup_auto_monitor))
	};
	
	self.state.camera_timezone_string = ko.computed(function() {
		return toString(self.state.camera_timezone_local_area()) + " GMT " + toString(self.state.camera_timezone_time_diff());
	});

	self.is_live = ko.observable(false);
	self.mac_address = ko.observable(toUpperCase(data.mac_address));

	self.toggle_recording = function () {
		self.state.recording_mode(!self.state.recording_mode());
	};

	self.recording_background = ko.computed(function () {
		return self.state.recording_mode()
			? "url('./img/onoff_rec_cam.png')"
			: "url('./img/blank_onoff_rec_cam.png')";
	}, self);

	self.panel = {
		show_scheduler: ko.computed(function () {
			var c = self.state.schedule_active()
				? "settings_panel camera_timer active_panel"
				: "settings_panel camera_timer inactive_panel";
			console.log("schedule_panel_available", schedule_panel_available);
			if(!schedule_panel_available){
				c =  "settings_panel camera_timer inactive_panel";
			}

			return c;
		}, self),

		email_alert_active: ko.computed(function () {
			var c = self.state.email_alert_active()
				? "settings_panel email_alerts active_panel"
				: "settings_panel email_alerts inactive_panel";
			console.log("ALERT schedule_panel_available", schedule_panel_available);
			if(!schedule_panel_available){
				c =  "settings_panel email_alerts inactive_panel";
			}

			return c;
		}, this)
	};

	self.delete_camera_enable = ko.observable(false);

	self.pass1 = ko.observable("");
	self.pass2 = ko.observable("");

	self.is_subscribed = ko.computed(function(){
		return true;
	})

	self.check_passwords = ko.computed(function () {
		var res = false;
		if(self.pass1().length >= 6 && self.pass1() === self.pass2()){
			res =  true;
		}

		return res;
	}, this);

	self.thumb_fallback_count = ko.observable(5);
}

/**
 * Object that is data container for knockout.js about cameras
 */
function CamerasView() {
	var self = this;

	/**
	 * All cameras for current user
	 * - change on this object is reflected directly on html tags trough data-bind attribute (knockout.js)
	 */
	self.cameras = ko.observableArray([]);

	/**
	 * Camera that is currently selected
	 */
	self.selected = ko.observable(new CameraData(empty_data));

	self.removeCamera = function (camera_id) {
		self.cameras.remove(function (camera) {
			return camera.id() == camera_id;
		});
	};

	self.subscription_end_time = ko.observable("Not subscribed");
	self.subscription_start_time = ko.observable("Not subscribed");

	self.user_email = ko.observable("Not available");
	self.phone_number = ko.observable("");
	self.telephone_co = ko.observable("");
}

/**
 * Instance that holds all data for cameras and selected camera for user
 * @type {CamerasView}
 */
 
