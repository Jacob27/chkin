function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

$(".batch-btn").on("click", function() {
    $("#url").text('-')

    var url = '-'

    btn_id = $(this).data('id')

	window.open("/batch/"+btn_id,'_blank')
})

$(".action-btn").on("click", function() {
    $("#url").text('-')
    $("#request").text('')
    $("#response").text('')

    var url = '-'

    btn_id = $(this).data('id')

    if (btn_id == 'gen_count') {
        url = "/dashboard/test/gen_count"
        req = {
            }
    }

    if (btn_id == 'get_counts') {
        url = "/dashboard/stats/counts"
        req = {
            }
    }

    if (btn_id == 'downloaded_mock') {
        url = "/dashboard/test/downloaded_mock"
        req = {
            }
    }

    if (btn_id == 'clear_usage') {
        url = "/dashboard/test/clear_usage"
        req = {
            }
    }

    if (url != '-') {
        $("#url").text(url)
        $("#request").text(JSON.stringify(req, null, '\t'))
        $.ajax({
            type: "POST",
            url: url,
            data: req,
            success: function(resp) {
                $("#response").text(JSON.stringify(resp, null, '\t'))
            },
            dataType: "json"
        });
    }
})
