d3.json('/dashboard/media/profile', function(data) {
    var size_format = d3.format(",.0f");

    $row = d3.select("#table-records").selectAll("tr")
        .data(data.message).enter()
        .append("tr")
    
    $row.append("td").text(function(d) { return d.title })
    $row.append("td").text(function(d) { return d.camera })
    $row.append("td").text(function(d) { return d.record_date })
    $row.append("td").text(function(d) { return d.duration })
    $row.append("td").text(function(d) { return size_format(d.size/1024.0) })

    jQuery.fn.dataTableExt.oSort['numeric-comma-asc']  = function(a,b) {
        var x = (a == "-") ? 0 : a.replace( /,/, "" );
        var y = (b == "-") ? 0 : b.replace( /,/, "" );
        x = parseFloat( x );
        y = parseFloat( y );
        return ((x < y) ? -1 : ((x > y) ?  1 : 0));
    };

    jQuery.fn.dataTableExt.oSort['numeric-comma-desc'] = function(a,b) {
        var x = (a == "-") ? 0 : a.replace( /,/, "" );
        var y = (b == "-") ? 0 : b.replace( /,/, "" );
        x = parseFloat( x );
        y = parseFloat( y );
        return ((x < y) ?  1 : ((x > y) ? -1 : 0));
    };

    var unsortableColumns = [];
    $('#datatable-table').find('thead th').each(function(){
        if ($(this).hasClass( 'no-sort')){
            unsortableColumns.push({"bSortable": false});
        }
        else if ($(this).hasClass( 'numeric-comma-sort')){
            unsortableColumns.push({"sType": "numeric-comma"});
        } else {
            unsortableColumns.push(null);
        }
    });


    $("#datatable-table").dataTable({
        "sDom": "<'row table-top-control'<'col-md-6 hidden-xs per-page-selector'l><'col-md-6'f>r>t<'row table-bottom-control'<'col-md-6'i><'col-md-6'p>>",
        "oLanguage": {
            "sLengthMenu": "_MENU_ &nbsp; records per page"
        },
        "iDisplayLength": 50,
        "aaSorting": [[ 0, "desc" ]],
        "aoColumns": unsortableColumns
    });

    $(".dataTables_length select").select2({
        minimumResultsForSearch: 10
    });
})
