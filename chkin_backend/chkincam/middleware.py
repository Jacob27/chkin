from chkincam.utils.exceptions import ErrorConvention
from chkincam.utils.views import ChkincamResponseMixin


class ViewExceptionMiddleware(ChkincamResponseMixin):
    def process_exception(self, request, exception):
        if isinstance(exception, ErrorConvention):
            return self.error(exception.msg)