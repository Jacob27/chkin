from django.contrib import admin
from apps.statistics.models import Statistics, Usage, SmsEmailNotificationLog


admin.site.register(Statistics)
admin.site.register(Usage)
admin.site.register(SmsEmailNotificationLog)
