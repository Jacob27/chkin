from django.db import models


class Statistics(models.Model):
    daily = models.IntegerField()
    weekly = models.IntegerField()
    monthly = models.IntegerField()

    def json(self):
        return {'daily': self.daily, 'weekly': self.weekly, 'monthly': self.monthly}


class Usage(models.Model):
    storage = models.ForeignKey(Statistics, related_name='storage_usage')
    downloaded = models.ForeignKey(Statistics, related_name='download_usage')

    def json(self):
        return {'storage': self.storage.json(), 'downloaded': self.downloaded.json()}


class SmsEmailNotificationLog(models.Model):
    """
    Log record for sms and email notification

    """
    camera = models.ForeignKey('camera.Camera')
    alert_type = models.CharField(max_length=45)
    timestamp = models.DateTimeField(auto_now_add=True)
