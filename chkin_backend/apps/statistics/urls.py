from django.conf.urls import url
from .views import SendEmailSmsAlert, TestMail, TestSms

urlpatterns = [
    url(r'^send_email_sms_alert', SendEmailSmsAlert.as_view(), name='some_view'),
    url(r'^test_mail$', TestMail.as_view(), name='test_mail'),
    url(r'^test_sms$', TestSms.as_view(), name='test_sms'),
]