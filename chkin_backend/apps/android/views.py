from apps.android.models import ChkinAPK
from apps.android.utils import GCMMessage, GCMConnection
from chkincam.utils.views import UserAuthView, CameraAuthView, UniversalAuthView

from logging import info as i, warning as w, error as e
from django.http import HttpResponseRedirect


import paho.mqtt.publish as publish

class WhatIsTheLatestApkVersion(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(WhatIsTheLatestApkVersion, self).dispatch(request, *args, **kwargs)
		build_id = request.REQUEST.get('build_id')
		apk = ChkinAPK.objects.filter(build_id=build_id).first()
		if not apk:
			return self.error("NOT APPLICABLE")
		else:
			return self.serve_result({
				"latest_version": apk.version,
			})


class DownloadLatestApk(UniversalAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(DownloadLatestApk, self).dispatch(request, *args, **kwargs)
		build_id = request.REQUEST.get('build_id')

		apk = ChkinAPK.objects.filter(build_id=build_id).order_by('-uploaded').first()

		redirect_url = "/storage/{relative_path}".format(
				relative_path=apk.apk_url
		)

		i("redirecting request to : " + redirect_url)
		redirect = HttpResponseRedirect("")
		redirect["X-Accel-Redirect"] = redirect_url
		return redirect;


class SendGcmToClient(CameraAuthView):
	http_method_names = ['get', 'post']

	def dispatch(self, request, *args, **kwargs):
		super(SendGcmToClient, self).dispatch(request, *args, **kwargs)
		# TODO why do we need it here?
		# expiry_in_seconds = float(request.REQUEST.get('build_id', 300.0))
		
		
		userID = "pub_client"
		password = "pub-passwd"
		push_token = request.REQUEST.get('push_token')
		payload = request.REQUEST.get('payload')
		auth = {'username':userID, 'password':password}
		publish.single(push_token, payload, hostname="67.207.205.6", port=1883, auth=auth)
		"""
		push_token = request.REQUEST.get('push_token')
		payload = request.REQUEST.get('payload')
		collapse_key = request.REQUEST.get('collapse_key')
		w(" > > > push_token={0}".format(push_token))
		gcm_message = GCMMessage(push_token, payload, collapse_key=collapse_key)
		gcm_conn = GCMConnection()
		gcm_conn.notify_device(gcm_message)
		"""
		return self.ok()
