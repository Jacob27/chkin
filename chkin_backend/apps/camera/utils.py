import base64
import pytz
import traceback
from celery.task.http import URL
from datetime import datetime, timedelta
from logging import info as i, warning as w, error as e

from django.contrib.sites.models import Site
from django.conf import settings
from django.core.mail import send_mail, EmailMessage

from apps.camera.models import Camera, VideoSegment
from apps.statistics.models import SmsEmailNotificationLog


def _send_blob(self, blob_info, content_type):
    blob_key = blob_info.key()
    w("setting blobkey={0}".format(str(blob_key)))
    response.headers[blobstore.BLOB_KEY_HEADER] = str(blob_key)
    response.headers["Content-Type"] = content_type
    response.status = ""


def _authorized_for_asset(self, blob_info, presented_key, model_attr):
    query = VideoSegment.query(model_attr == blob_info.key())
    segment = query.get()
    if not segment:
        e("/video(id={id}) segment missing".format(id=id))
        return False
    owner = segment.owner.get()
    if owner.key == presented_key:
        # this is camera itself
        w("authorized for asset - camera")
        return True
    if not isinstance(owner, Camera):
        e("/video(id={id}) segment owner is not Camera".format(id=id))
        return False
    if owner.owner == presented_key:
        # segment's camera owner
        w("authorized for asset - user")
        return True
    w("NOT authorized for asset")
    return False


# def _remove_camera_from_user(self, camera):


class EmailSmsViewMixin(object):
    def sms_mail_quota_check(self, camera, alert_type):
        """
        looks for the notification of same kind in under 30 mins.
        if not, returns True
        """
        try:
            log = SmsEmailNotificationLog.objects.get_or_create(camera=camera, alert_type=alert_type)
        except SmsEmailNotificationLog.DoesNotExist:
            SmsEmailNotificationLog.create(camera=camera, alert_type=alert_type)
            return True  # alert can be sent

        try:
            if log.timestamp + timedelta(0, settings.NOTIFICATION_INTERVAL * 60) <= datetime.now():
                i("The time difference is more than {0} min.".format(settings.NOTIFICATION_INTERVAL))
                log.timestamp = datetime.now()
                log.put()
                return True  # alert can be sent
            else:
                i("The time difference is less than {0} min.".format(settings.NOTIFICATION_INTERVAL))
                return False  # alert can not be sent

        except Exception as ex:
            i("Exception in time difference calculation, Sending the notification Anyway")
            i(ex)
            return True  # alert can be sent

    def _send_email_sms_alert(self, alert_type, receiver, phone_number, telephone_co, camera_name, mac_address,
                              camera, camera_timezone):

        subject = ""
        body = ""
        html = ""
        email_alert = False
        sms_alert = False

        if alert_type == "motion_event_alert_email":
            if self.sms_mail_quota_check(camera, alert_type):
                email_alert = True
                subject = "Motion Alert"
                time_str = datetime.now(pytz.timezone(camera_timezone)).strftime('%I:%M:%S %p on %A %B %d')

                video_segment = VideoSegment.objects.filter(owner=camera, deleted=False).first()
                if video_segment is not None:
                    img_url = "image?id={id}".format(id=base64.urlsafe_b64encode(str(video_segment.thumbnail_url)))
                else:
                    img_url = ""

                html = """<html><body><div><font color="#000000"><b>Chk-In Cam detected motion event on {camera_name} at {motion_time}. </b></font><br><br><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">Here is a snapshot at {motion_time}. <br><div style="width:500px;line-height:140%;display:inline-block"><br></div><a href="http://{host}:{port}" target="_blank"><img alt="Activity Detected!" src="http://{host}:{port}/{img_url}" border="0"></a> <br><br>CVR Subscribers: Click on the image to connect to ChkInCam and view the recorded footage for any unusual activities.</div></div><div><font color="#666666"><br></font></div><div><div style="color:rgb(102,102,102);font-size:13px;width:580px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">You are receiving this email because you asked to be notified when your camera detects activities. You can change your notification&nbsp;<a href="http://{host}:{port}" style="text-decoration:none" target="_blank">settings</a>&nbsp;by logging in to your chkincam account.</div><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">&copy; Chk-In&nbsp;</div><span style="color:rgb(102,102,102)">&nbsp;</span><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:80px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">Have comments or questions? Email&nbsp;<a href="http://{host}:{port}/support" style="text-decoration:none" target="_blank">Chk-In Support</a>.&nbsp;<br><br><a href="http://{host}:{port}/privacy" style="text-decoration:none" target="_blank">Privacy Policy</a>&nbsp;|&nbsp;<a href="http://{host}:{port}/TOS" style="text-decoration:none" target="_blank">Terms of Service</a></div><font color="#666666"><br></font></div></body></html>""".format(
                    camera_name=camera_name, motion_time=time_str, host=settings.SERVER_HOSTNAME,
                    port=settings.SERVER_PORT,
                    img_url=img_url)
            else:
                i("Already a notification has been sent under {0} mins".format(settings.NOTIFICATION_INTERVAL))

        if alert_type == "loud_noise_alert_email":
            if self.sms_mail_quota_check(camera, alert_type):
                email_alert = True
                subject = "Noise Alert"
                time_str = datetime.now(pytz.timezone(camera_timezone)).strftime('%I:%M:%S %p on %A %B %d')

                video_segment = VideoSegment.objects.filter(owner=camera, deleted=False).first()
                if video_segment is not None:
                    img_url = "image?id={id}".format(id=base64.urlsafe_b64encode(str(video_segment.thumbnail_url)))
                else:
                    img_url = ""
                html = """<html><body><div><b style="color:rgb(0,0,0)">Chk-In Cam detected loud noise on {camera_name} at {noise_time}.&nbsp;</b><br></div><div><br><div style="width:500px;color:rgb(102,102,102);line-height:21px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">Here is a snapshot at {noise_time}.&nbsp;<br><div style="width:500px;display:inline-block"><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#666666">&nbsp;For Motion Event</font></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#666666"><br></font></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#000000"><b>Chk-In Cam detected loud noise event on {camera_name} at {noise_time}.&nbsp;</b></font><br><br><div style="width:500px;color:rgb(102,102,102);line-height:21px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">Here is a snapshot at 7:09:41 AM on Thursday, December 18.&nbsp;</div></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><div style="width:500px;color:rgb(102,102,102);line-height:21px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block"><div style="width:500px;display:inline-block"><br></div><a href="http://{host}:{port}" target="_blank"><img alt="Activity Detected!" src="http://{host}:{port}/{img_url}" border="0"></a><br><br>CVR Subscribers: Click on the image to connect to ChkInCam and view the recorded footage for any unusual activities.</div></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><font color="#666666"><br></font></div><div style="color:rgb(34,34,34);font-family:arial,sans-serif;font-size:small;line-height:normal;word-spacing:0px"><div style="color:rgb(102,102,102);font-size:13px;width:580px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">You are receiving this email because you asked to be notified when your camera detects activities. You can change your notification&nbsp;<a href="http://{host}:{port}/" style="text-decoration:none" target="_blank">settings</a>&nbsp;by logging in to your chkincam account.</div><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">&copy; Chk-In&nbsp;</div><span style="color:rgb(102,102,102)">&nbsp;</span><br style="color:rgb(102,102,102)"><br style="color:rgb(102,102,102)"><div style="color:rgb(102,102,102);font-size:13px;line-height:15.6000003814697px;padding-right:80px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;word-spacing:1px;display:inline-block">Have comments or questions? Email&nbsp;<a href="http://{host}:{port}/support" style="text-decoration:none" target="_blank">Chk-In Support</a>.&nbsp;<br><br><a href="http://{host}:{port}/privacy" style="text-decoration:none" target="_blank">Privacy Policy</a>&nbsp;|&nbsp;<a href="http://{host}:{port}/TOS" style="text-decoration:none" target="_blank">Terms of Service</a></div></div></div><br></div></div></body></html>""".format(
                    camera_name=camera_name, noise_time=time_str, host=settings.SERVER_HOSTNAME,
                    port=settings.SERVER_PORT,
                    img_url=img_url)
            else:
                i("Already a notification has been sent under {0} mins".format(settings.NOTIFICATION_INTERVAL))

        if alert_type == "offline_alert_email":
            if self.sms_mail_quota_check(camera, alert_type):
                email_alert = True
                subject = "Offline Alert"
                time_str = datetime.now(pytz.timezone(camera_timezone)).strftime('%I:%M:%S %p on %A %B %d')
                html = """<html><body><div>"{camera_name}" is offline. <br><br><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">"{camera_name}" &nbsp;went offline at {offline_time}.  <br><br>The lost of connection may be the result of power outage, Internet connection interruption, or someone disconnect the camera.</div></div><div><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block"><br></div></div><div><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block">If you subscribes to the CVR service, please connect to Chk-In cam to review earlier footage for unusual activities.</div></div><div><div style="width:500px;color:rgb(102,102,102);line-height:140%;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:15px;word-spacing:1px;display:inline-block"><br></div></div><div><font color="#666666"><div style="width:580px;line-height:120%;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:13px;word-spacing:1px;display:inline-block">You are receiving this email because you asked to be notified when your camera goes offline. You can change your notification <a style="text-decoration:none" href="http://{host}:{port}" target="_blank">settings</a> by logging in to your chkincam account.</div><br><br><div style="line-height:120%;padding-right:20px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:13px;word-spacing:1px;display:inline-block">&copy; Chk-In&nbsp;</div>&nbsp;<br><br><div style="line-height:120%;padding-right:80px;padding-left:20px;font-family:avenir,'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:13px;word-spacing:1px;display:inline-block">Have comments or questions? Email <a style="text-decoration:none" href="http://{host}:{port}/support" target="_blank">Chk-In Support</a>. <br><br><a style="text-decoration:none" href="http://{host}:{port}/privacy" target="_blank">Privacy Policy</a> | <a style="text-decoration:none" href="http://{host}:{port}/TOS" target="_blank">Terms of Service</a></div></font><br></div></body></html>""".format(
                    camera_name=camera_name, offline_time=time_str, host=settings.SERVER_HOSTNAME,
                    port=settings.SERVER_PORT)
            else:
                i("Already a notification has been sent under {0} mins".format(settings.NOTIFICATION_INTERVAL))

        if alert_type == "motion_event_alert_push":
            if self.sms_mail_quota_check(camera, alert_type):
                sms_alert = True
                subject = "Motion Alert"
            else:
                i("Already a notification has been sent under {0} mins".format(settings.NOTIFICATION_INTERVAL))

        if alert_type == "loud_noise_alert_push":
            if self.sms_mail_quota_check(camera, alert_type):
                sms_alert = True
                subject = "Noise Alert"
            else:
                i("Already a notification has been sent under {0} mins".format(settings.NOTIFICATION_INTERVAL))

        if alert_type == "offline_alert_push":
            if self.sms_mail_quota_check(camera, alert_type):
                sms_alert = True
                subject = "Offline Alert"
                body = """Your camera "{camera_name}"({mac_address}) got offline.""".format(camera_name=camera_name,
                                                                                            mac_address=mac_address)
            else:
                i("Already a notification has been sent under {0} mins".format(settings.NOTIFICATION_INTERVAL))

        if (email_alert or sms_alert) and subject == "":
            e("alert type not reckognized, returning : NOT APPLICABLE")
            return self.error("NOT APPLICABLE")

        if email_alert:
            try:
                e("Sending email with subject : {0} , receiver : {1}".format(repr(subject), repr(receiver)))
                send_mail(subject, body, settings.SERVER_SENDER_ADDRESS, [receiver], html_message=html)
            except:
                ## Verry dangerous to skip the exception
                e("Exception raised !!!")
                traceback.print_exc()

        if sms_alert == True:
            if phone_number and telephone_co:
                e("sms_alert phone: {0}".format(repr(phone_number)))
                e("sms_alert telephone_co: {0}".format(repr(telephone_co)))
                new_receiver = phone_number + telephone_co
                e("sms_alert new_receiver: {0}".format(repr(new_receiver)))

                email = EmailMessage(subject, body, settings.SERVER_SENDER_ADDRESS,
                                     [new_receiver], cc="seaman9297@gmail.com")
                email.send()

        return self.ok()
