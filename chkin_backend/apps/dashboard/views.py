from django.shortcuts import render
from django.views.generic import DetailView, TemplateView, View
from django.http import HttpResponse
from django.template import RequestContext, loader
from chkincam.utils.views import ChkincamResponseMixin
from datetime import datetime, timedelta
import logging
import csv
import apps.dashboard.utils as info
from apps.users.models import User
from apps.camera.models import Camera
from django.core.urlresolvers import resolve

## TODO move log file path to settings please, causing crashes on other environments
## logging.basicConfig(filename="/work/dashboard.log",level=logging.DEBUG)
# Create your views here.
class DashboardView(TemplateView):
    template_name = 'dashboard/index.html'
    template = loader.get_template(template_name)
    #model = get_user_model()
    def dispatch(self, request, *args, **kwargs):
        super(DashboardView, self).dispatch(request, *args, **kwargs)
        return HttpResponse(self.template.render({'active_page': 'dashboard'}))

class UserlistView(TemplateView):
    template_name="dashboard/users.html"
    template=loader.get_template(template_name)

    def dispatch(self, request, *args, **kwargs):
        super(UserlistView, self).dispatch(request, *args, **kwargs)
        return HttpResponse(self.template.render({'active_page': 'users'}))

class CameralistView(TemplateView):
    template_name="dashboard/cameras.html"
    template=loader.get_template(template_name)
    
    def dispatch(self, request, *args, **kwargs):
        super(CameralistView, self).dispatch(request, *args, **kwargs)
        return HttpResponse(self.template.render({'active_page': 'cameras'}))
    
class FilmsView(TemplateView):
    template_name="dashboard/media.html"
    template=loader.get_template(template_name)
    
    def dispatch(self, request, *args, **kwargs):
        super(FilmsView, self).dispatch(request, *args, **kwargs)
        return HttpResponse(self.template.render({'active_page': 'media'}))
    
class BlobMissingView(TemplateView):
    template_name = "dashboard/blob_missing.html"
    template = loader.get_template( template_name )
    
    def dispatch(self, request, *args, **kwargs):
        super(BlobMissingView, self).dispatch(request, *args, **kwargs)
        return HttpResponse(self.template.render({'active_page': 'blobmissing'}))


# JSON response
class StatsCountsView(ChkincamResponseMixin, View):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        return self.ok()

class StatsHistogramView(ChkincamResponseMixin, View):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        return self.ok()

class UsersListAPI(ChkincamResponseMixin, View):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        resp = []
        for ent in User.objects.filter(deleted=False)[0:500]:
            if ent.usage != None:
                cost = {
                'daily': info.calculate_cost(ent.usage.storage.daily, ent.usage.downloaded.daily),
                'weekly': info.calculate_cost(ent.usage.storage.weekly, ent.usage.downloaded.weekly),
                'monthly': info.calculate_cost(ent.usage.storage.monthly, ent.usage.downloaded.monthly),
                }
                usage = ent.usage.json()
            else:
                cost = info.default_cost
                usage = info.default_usage

            resp.append({'key': ent.session_key, 'id': ent.name, 'camera_count': ent.camera_count,
                         'media_count': ent.media_count, 'storage_used': ent.storage_used,
                         'deleted': ent.deleted == True, 'usage': usage, 'cost': cost})
        return self.serve_result({'data':resp})

class DeleteUserAPI(ChkincamResponseMixin, View):
    http_method_names = ['get', 'post']

    def dispatch(self, request, *args, **kwargs):
        email = str(request.REQUEST.get('email')).lower()
        user = User.objects.get(email = email)
        # taskqueue.add(url='/delete_user_task', params={'user_key': (user.key.urlsafe())})

        # "deleting" the user's cameras
        for cam in Camera.objects.filter(owner = user):
            cam.deleted = True
            cam.save()

        # "deleting" user
        user.camera_count = 0
        user.media_count = 0
        user.storage_used = 0
        user.deleted = True
        user.save()

        return self.ok()

class CSVAPI(DetailView):
    def  dispatch(self, request, *args, **kwargs):
        pathinfo = request.path_info.split('/')
        filename = pathinfo[4]
        report = pathinfo[3]
        response = HttpResponse(content_type = 'application/csv')
        writer = csv.writer(response)

        if report == 'users':
            writer.writerow(["User", "# of Cameras", "# of Media", "Storage Used", "$ Daily", "$ Weekly", "$ Monthly",
                             "Storage Daily", "Storage Weekly", "Storage Monthly", "Downloaded Daily",
                             "Downloaded Weekly", "Downloaded Monthly"])
            for ent in info.UserInfo.summary():
                writer.writerow([ent['id'], ent['camera_count'], ent['media_count'], ent['storage_used'],
                                 ent['usage']['storage']['daily'], ent['usage']['storage']['weekly'],
                                 ent['usage']['storage']['monthly'], ent['usage']['downloaded']['daily'],
                                 ent['usage']['downloaded']['weekly'], ent['usage']['downloaded']['monthly'],
                                 ent['cost']['daily'], ent['cost']['weekly'], ent['cost']['monthly']])

        return response
