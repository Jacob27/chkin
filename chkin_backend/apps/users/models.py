from datetime import datetime, timedelta
from annoying.fields import JSONField
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _
from apps.statistics.models import Usage
from logging import debug as d, info as i, warning as w, error as e, critical as c

from django.contrib.auth.models import AbstractUser
from apps.camera.models import GCMCommand

import paho.mqtt.publish as publish

class User(AbstractUser):
	"""
	Extending custom user EmailUser model
	"""

	subscription = models.ForeignKey('Subscription', related_name='user_subscription', blank=True, null=True)
	subscription_start_time = models.DateTimeField(blank=True, null=True)
	subscription_end_time = models.DateTimeField(blank=True, null=True)
	phone_number = models.CharField(max_length=20, blank=True)
	telephone_co = models.CharField(max_length=45, blank=True)
	camera_count = models.IntegerField(blank=True, null=True)
	media_count = models.IntegerField(blank=True, null=True)
	storage_used = models.IntegerField(blank=True, null=True)
	deleted = models.BooleanField(default=False)
	usage = models.ForeignKey(Usage, related_name='user_usage', blank=True, null=True)

	challenge = models.CharField(max_length=45, blank=True)
	session_key = models.CharField(max_length=100, blank=True)
	session_created = models.DateTimeField(blank=True, null=True)

	def get_full_name(self):
		if self.first_name and self.last_name:
			return self.first_name + ' ' + self.last_name
		else:
			return super(User, self).get_full_name()

	get_full_name.short_description = 'Full name'

	@property
	def name(self):
		return self.email

	def is_subscribed(self, user_key):
		"""returns True if subscription available
		args:
		- `user_key`
		type:
		- `ndb.Key`
		entity:
		- `User`
		"""
		# user = user_key.get()
		if self.user.subscription_end_time:
			return self.user.subscription_end_time > datetime.now()
		return False

	def send_gcm_message(self, payload, collapse_key, expiry_in_seconds=300.0):
		push_token = self.email
		userID = "pub_client"
		password = "pub-passwd"
		payload = payload
		auth = {'username':userID, 'password':password}
		i("Sending message to device with id : " + push_token + " , payload : " + payload)
		publish.single(push_token, payload, hostname="67.207.205.6", port=1883, auth=auth)

		command = UserCommand()
		command.user = self
		command.utc_expiry = datetime.utcnow() + timedelta(0, expiry_in_seconds)
		command.payload = payload
		command.save()


class PaymentRecord(models.Model):
	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_paymentrecord')
	token = models.CharField(max_length=45)
	start = models.DateTimeField()
	end = models.DateTimeField()
	price = models.FloatField()
	plan_type = models.CharField(max_length=45)
	plan_days = models.IntegerField()
	checkout_setup_response = JSONField(blank=True, null=True)
	transaction_details_response = JSONField(blank=True, null=True)
	execution_response = JSONField(blank=True, null=True)


class Packet(models.Model):
	name = models.CharField(max_length=45, blank=True)


class Subscription(models.Model):
	packets = models.ForeignKey(Packet, related_name='subscription', blank=True)


class UserCommand(models.Model):
	user = models.ForeignKey(User, related_name='gcm_command')
	utc_expiry = models.DateTimeField()
	payload = JSONField(blank=True, null=True)


class ClientSession(models.Model):
	owner = models.ForeignKey(User, related_name='client_session')
	user_agent = models.CharField(max_length=255, blank=True, null=True)
	session_key = models.CharField(max_length=100, blank=True)
	expires_at = models.DateTimeField(blank=True, null=True)
	ip_address = models.CharField(max_length=30, blank=True)
	logged_in = models.DateTimeField(blank=True, null=True)
	logged_out = models.DateTimeField(blank=True, null=True)

