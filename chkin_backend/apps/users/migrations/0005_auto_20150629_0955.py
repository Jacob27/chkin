# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_clientsession'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientsession',
            name='expires_at',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='clientsession',
            name='logged_in',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='clientsession',
            name='logged_out',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='clientsession',
            name='user_agent',
            field=models.CharField(max_length=45, null=True, blank=True),
        ),
    ]
